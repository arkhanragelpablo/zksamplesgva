package es.gva.aplicacion.arquetipobase.integration.impl;

import es.gva.aplicacion.arquetipobase.integration.api.CorreoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Integración con el servicio de correo electrónico.
 * 
 * @author mmartinez at http://www.organizacion.web[ORGANIZACION]
 */
@Service
public class CorreoServiceImpl implements CorreoService {

  /** Utilidad de Spring para el envío de correos */
  private final JavaMailSender servicioEnvioCorreo;

  /** Implementación de logging para mostrar trazas. */
  private static final Logger LOG = LoggerFactory.getLogger(CorreoServiceImpl.class);

  /**
   * Inyección de dependencias de Spring.
   * 
   * <p>
   * El {@link JavaMailSender} es opcional para evitar errores si el sistema de
   * envío de correo no se utiliza.
   * </p>
   * 
   * @param servicioEnvioCorreo Servicio de envío de correo de Spring.
   */
  @Autowired
  public CorreoServiceImpl(@Lazy JavaMailSender servicioEnvioCorreo) {
    this.servicioEnvioCorreo = servicioEnvioCorreo;
  }

  /**
   * {@inheritDoc}
   * 
   * <p>
   * Utiliza el servicio de correo configurado en Spring {@link JavaMailSender}.
   * </p>
   */
  public void enviar(String asunto, String cuerpo, String para) {

    try {

      MimeMessage mensaje = servicioEnvioCorreo.createMimeMessage();
      MimeMessageHelper asistenteMensaje = new MimeMessageHelper(mensaje, true);
      asistenteMensaje.setSubject(asunto);
      asistenteMensaje.setText(cuerpo);
      asistenteMensaje.setTo(para);
      // Crea un fichero temporal vacío para incluirlo como adjunto en el correo
      asistenteMensaje.addAttachment("adjunto.txt",
          File.createTempFile("adjunto", "txt"));
      this.servicioEnvioCorreo.send(mensaje);

    } catch (MessagingException me) {
      // Esta excepción engloba errores al construir o enviar el mensaje
      LOG.warn("Error en el mensaje de correo o su envío.", me);
    } catch (IOException ioe) {
      LOG.warn("Error al crear el adjunto del mensaje de correo.", ioe);
    }
  }

}

