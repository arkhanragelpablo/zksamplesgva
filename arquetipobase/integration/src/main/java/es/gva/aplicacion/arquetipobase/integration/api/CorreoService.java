/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.integration.api;

/**
 * Integración con el servicio de correo electrónico.
 * 
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */

public interface CorreoService {

  /**
   * Envia un correo con cierto asunto, cuerpo y destinatario.
   * 
   * @param asunto Asunto del correo.
   * @param cuerpo Cuerpo del correo.
   * @param para Destinatario del correo.
   */
  void enviar(String asunto, String cuerpo, String para);
  
}
