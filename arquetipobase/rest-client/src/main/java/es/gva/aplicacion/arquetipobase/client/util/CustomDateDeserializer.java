package es.gva.aplicacion.arquetipobase.client.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Deserialización de fecha (dd/MM/yyyy) de Json a {@link Date}.
 *
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */

public class CustomDateDeserializer extends JsonDeserializer<Date> {

  /** Formateador de fecha */
  private final SimpleDateFormat formatter =
      new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));

  /**
   * Convierte un tipo de fecha de nodo Json en entidad.
   */
  @Override
  public Date deserialize(JsonParser jsonParser, DeserializationContext context)
      throws IOException {

    // El nodo Json contiene la fecha en formato texto
    JsonNode nodoJsonFecha = jsonParser.getCodec().readTree(jsonParser);
    String textoFecha = nodoJsonFecha.asText();
    try {

      // La fecha en texto tiene el formato dd/MM/yyyy
      return formatter.parse(textoFecha);

    } catch (ParseException pe) {
      throw new IOException("Error al deserializar la fecha " + textoFecha, pe);
    }
  }

}

