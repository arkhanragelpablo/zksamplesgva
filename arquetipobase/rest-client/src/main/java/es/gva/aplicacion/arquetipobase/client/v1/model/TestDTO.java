package es.gva.aplicacion.arquetipobase.client.v1.model;

import java.io.Serializable;

/**
 * Entidad <i>Test</i>
 *
 * <p>
 * Entidad de test DTO para cliente de servicio REST
 * </p>
 *
 * @author INDRA
 *
 */

public class TestDTO implements Serializable {

  private static final long serialVersionUID = -622398920617085054L;

  private Long id;
  private String nombre;
  
  
  public TestDTO(Long id, String nombre) {
	super();
	this.id = id;
	this.nombre = nombre;
  }

  public Long getId() {
	return id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public String getNombre() {
	return nombre;
  }

  public void setNombre(String nombre) {
	this.nombre = nombre;
  }

  @Override
  public String toString() {
	return "Test [id=" + id + ", nombre=" + nombre + "]";
  }

}
