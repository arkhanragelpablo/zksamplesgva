package es.gva.aplicacion.arquetipobase.client.v1.api;

import es.gva.aplicacion.arquetipobase.client.v1.model.TestDTO;

/**
 * Cliente del servicio web REST
 * 
 * @author indra
 */

public interface TestRestService {

  /**
   * 
   * @return Test obtenida.
   */
  TestDTO show(long id);

}
