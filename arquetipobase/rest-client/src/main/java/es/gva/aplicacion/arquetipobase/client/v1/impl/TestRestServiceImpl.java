package es.gva.aplicacion.arquetipobase.client.v1.impl;

import es.gva.aplicacion.arquetipobase.client.v1.api.TestRestService;
import es.gva.aplicacion.arquetipobase.client.v1.model.TestDTO;
import io.tracee.binding.springhttpclient.TraceeClientHttpRequestInterceptor;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * Cliente del servicio web REST de test.
 * 
 * <p>
 * Establece el timeout de conexión y lectura a un máximo (milisegundos)
 * definido un interceptor para el identificador de trazabilidad.
 * </p>
 * 
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */

public class TestRestServiceImpl implements TestRestService {

  /** Dirección del servicio REST de Test */
  private final String direccion;

  /** Utilidad de Spring para realizar peticiones REST */
  private final RestTemplate restTemplate;

  /**
   * Establece el tiempo de espera y el traceo de las llamadas del cliente REST.
   * 
   * @param direccion Dirección del servicio REST de test.
   * @param tiempoEspera Tiempo espera conexión y recepción del servicio.
   */
  public TestRestServiceImpl(String direccion, Integer tiempoEspera) {

    this(direccion, tiempoEspera, new RestTemplateBuilder());
  }

  /**
   * Establece el tiempo de espera y el traceo de las llamadas del cliente REST.
   * 
   * <p>
   * Permite establecer el constructor de peticiones REST.
   * </p>
   * 
   * @param direccion Dirección del servicio REST de test.
   * @param tiempoEspera Tiempo espera conexión y recepción del servicio.
   * @param restTemplateBuilder Constructor de peticiones REST opcional.
   */
  public TestRestServiceImpl(String direccion, Integer tiempoEspera, RestTemplateBuilder restTemplateBuilder) {

    RestTemplateBuilder constructor = restTemplateBuilder;

    // Tiempo máximo de espera (timeout) del cliente (configurable)
    if (tiempoEspera != null) {
      constructor = constructor.setConnectTimeout(tiempoEspera);
      constructor = constructor.setReadTimeout(tiempoEspera);
    }

    // Incluye el identificador de trazabilidad en la petición
    constructor = constructor.additionalInterceptors(
        Collections.singletonList(new TraceeClientHttpRequestInterceptor()));

    this.restTemplate = constructor.build();
    this.direccion = direccion;
    
  }

  /**
   * Obtiene una @{link TestDTO} del servicio web REST.
   * 
   * @param id Identificador de TestDTO a obtener.
   * @return TestDTO.
   */
  @Override
  public TestDTO show(long id) {

    try {

      return restTemplate.getForObject(direccion + "/{id}", TestDTO.class, id);

    } catch (RestClientException rce) {
      throw new RestClientException(
          "Error servicio web REST al obtener TestDTO " + id, rce);
    }
  }

}

