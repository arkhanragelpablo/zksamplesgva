/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

//import es.gva.aplicacion.arquetipobase.exceptions.RecursoNoExisteException;
import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.exception.NombreIncorrectoException;
import es.gva.aplicacion.arquetipobase.service.api.model.DummyDTO;

/**
 * Controlador del servicio REST de test en JSON.
 *
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 */

@RestController
@RequestMapping(value = "/api/test", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestJsonController {

  /** Implementación de _logging_ para mostrar trazas. */
  private static final Logger LOG = LoggerFactory.getLogger(TestJsonController.class);

  /** API del servicio para la entidad */
  private final TestService testService;

  /** Interfaz para resolver mensajes en la aplicación */
  private final MessageSource messageSource;

  /**
   * Constructor de la clase.
   *
   * @param testService Servicio de test.
   */
  public TestJsonController(TestService testService, MessageSource messageSource) {
	  
    this.testService = testService;
    this.messageSource = messageSource;
    
  }

  /**
   * Obtiene la instancia de dummy cuyo identificador coincide con el
   * identificador especificado en la URL de la petición.
   *
   * <p>
   * Este método se ejecutará siempre en todas las peticiones.
   * </p>
   *
   * @param id Identificador del dummy
   * @return instancia de {@link DummyDTO}
   */
  @ModelAttribute
  public DummyDTO getDummy(@PathVariable(value = "dummy", required = false) Long id) {

	  DummyDTO dum = null;

    if (id != null) {
      // Show, update, delete: obtener la entidad DummyDTO
    	//dum = DummyDTO.toModel(testService.findOne(id));
    	dum = new DummyDTO();
    	dum.setId(1L);
    	dum.setNombre("Dumbo");
    	/*
      if (dum == null) {
        // No se ha podido obtener el recurso solicitado
        throw new RecursoNoExisteException(
            String.format("Dummy con identificador '%s' no encontrado", id));
      }
      */
    }

    return dum;
  }

  /**
   * Crear una nueva entidad.
   *
   * <p>
   * En el caso de que la creación se ejecute de forma satisfactoria, se
   * devolverá el código de estado 201 y se incluye el header Location con la
   * URL al nuevo recurso.
   * </p>
   *
   * @param dummy nuevo instancia de DummyDTO cuyo estado se ha actualizado con
   *        los datos recibidos en un mensaje JSON.
   * @param result contenedor de errores encontrados en el proceso de
   *        transformación de los datos recibidos. Si no hay errores el
   *        contenedor estará vacío.
   * @return en la cabecera se incluye: el HTTP Status Code y el Location con la
   *         URI del nuevo recurso.
   * @throws NombreIncorrectoException
   */
  @PostMapping
  public ResponseEntity<?> create(@Valid @RequestBody DummyDTO dum, BindingResult result) throws NombreIncorrectoException {

    // No proporcionar identificador en la creación
    if (dum.getId() != null) {
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    // Error en la petición Json
    if (result.hasErrors()) {
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    DummyDTO nuevoDum = testService.save(DummyDTO.toModel(dum));

    // Crear la URL para visualización de los datos actualizados
    UriComponents showURI = MvcUriComponentsBuilder
        .fromMethodCall(
            MvcUriComponentsBuilder.on(TestJsonController.class).show(null))
        .buildAndExpand(nuevoDum.getId()).encode();

    return ResponseEntity.created(showURI.toUri()).build();
  }

  /**
   * Obtener el DummyDTO dum con el identificador indicado.
   *
   * @param DummyDTO dum obtenida de la base de datos por el método
   *        {@link #getDummy(Long)} anotado con @ModelAtribute
   * @return mensaje con los datos del DummyDTO
   */
  @GetMapping(value = "/{dummy}")
  @ResponseStatus(HttpStatus.OK)
  public DummyDTO show(@ModelAttribute DummyDTO dum) {
    return dum;
  }


  /**
   * Actualizar una entidad
   *
   * <p>
   * Actualiza el estado de la entidad {@link DummyDTO} con los datos del mensaje
   * JSON recibido en el cuerpo de la petición HTTP.<br/>
   * En el caso de que la creación se ejecute de forma satisfactoria, se
   * devolverá el código de estado 200.
   * </p>
   *
   * @param dumGuardada DummyDTO obtenida de la base de datos por el método
   *        {@link #getDummy(Long)} anotado con @ModelAtribute
   * @param dum DummyDTO obtenido de la base de datos por el método
   *        {@link #getDummy(Long)} anotado con @ModelAtribute
   * @param result contenedor de errores encontrados en el proceso de
   *        transformación de los datos recibidos. Si no hay errores el
   *        contenedor estará vacío.
   * @return en la cabecera se incluye: el HTTP Status Code
   * @throws NombreIncorrectoException
   */
  @PutMapping(value = "/{dummy}")
  public ResponseEntity<?> update(@ModelAttribute DummyDTO dumGuardada,
      @Valid @RequestBody DummyDTO dum, BindingResult result) throws NombreIncorrectoException {

    // Error en la petición Json
    if (result.hasErrors()) {
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    // No está permitido modificar el identificador
    dum.setId(dumGuardada.getId());
    testService.save(DummyDTO.toModel(dum));
    return ResponseEntity.ok().build();
  }

  /**
   * Borrar los datos de una entidad
   *
   * <p>
   * Borra la entidad cuyo identificador coincide con el identificador
   * especificado en la URL de la petición<br/>
   * En el caso de que el borrado se ejecute de forma satisfactoria, se
   * devolverá el código de estado 200.
   * </p>
   *
   * @param dum DummyDTO obtenido de la base de datos por el método
   *        {@link #getDummy(Long)} anotado con @ModelAtribute
   */
  @DeleteMapping(value = "/{dummy}")
  @ResponseStatus(HttpStatus.OK)
  public void delete(@ModelAttribute DummyDTO dum) {
	  testService.delete(dum.getId());
  }

  /**
   * Captura y gestiona las excepciones de tipo
   * {@link NombreIncorrectoException}
   *
   * @param req Petición
   * @param e Error.
   * @param locale Idioma
   */
  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(NombreIncorrectoException.class)
  public void sendNombreIncorrectoExceptionStatusCode(
      HttpServletRequest req, NombreIncorrectoException e,  Locale locale) {

    String error = this.messageSource.getMessage(
        "error_nombreIncorrectoException", new Object[] {e.getDummy().getNombre()}, locale);

    LOG.error(error, e);

    // La respuesta incluirá el código de estado HTTP 409
  }
  

}
