/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.jackson;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;
/*
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Date;
import java.util.Set;
*/

/**
 * Configuración de deserialización de la mascota.
 *
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */
@JsonPropertyOrder({"id", "nombre"})
public abstract class AbstractDummyJsonMixin {

  /** Tipo de mascota */
  // Utiliza su propia clase de deserialización
  //@JsonProperty("tipoMascota")
  //@JsonDeserialize(using = TipoMascotaDeserializer.class)
  //private TipoMascota tipo;

  /** Visitas de la mascota */
  // No se serializa
  //@JsonIgnore
  //private Set<Visita> visitas;

  /** Propietario de la mascota */
  // Utiliza su propia clase de deserialización
  //@JsonDeserialize(using = PropietarioDeserializer.class)
  // Se ignoran ciertas propiedades de la relación
  //@JsonIgnoreProperties({"version", "direccion", "ciudad", "telefono", "mascotas"})
  //private Propietario propietario;

  /** Fecha de nacimiento de la mascota */
  // Formato personalizado de fecha
  //@JsonFormat(pattern = "dd/MM/yyyy")
  //private Date fechaNacimiento;

  /** Fecha de fallecimiento de la mascota */
  // Formato personalizado de fecha
  //@JsonFormat(pattern = "dd/MM/yyyy")
  //private Date fechaFallecimiento;
  
}
