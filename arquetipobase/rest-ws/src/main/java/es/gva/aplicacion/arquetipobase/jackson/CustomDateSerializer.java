/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Serializador personalizado para la clase {@link Date}
 *
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 *
 */
public class CustomDateSerializer extends JsonSerializer<Date> {

  /** Formateador de fecha */
  private final SimpleDateFormat formatter =
      new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));

  /**
   * {@inheritDoc}
   * 
   * Serializa la fecha en formato español.
   */
  @Override
  public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2)
      throws IOException {
    gen.writeString(formatter.format(value));
  }

}

