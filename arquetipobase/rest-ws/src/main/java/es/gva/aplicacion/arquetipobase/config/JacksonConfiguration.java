/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.config;

import es.gva.aplicacion.arquetipobase.jackson.AbstractDummyJsonMixin;
import es.gva.aplicacion.arquetipobase.jackson.CustomDateSerializer;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * Configuración de Jackson
 *
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 */
@Configuration
public class JacksonConfiguration {

  /**
   * Módulo Jackson para soporte de la serialización y deserialización JSON de
   * tipos de datos y propiedades de Hibernate.
   *
   * @return {@link Hibernate5Module}
   */

  @Bean
  public Hibernate5Module hibernate5Module() {
    Hibernate5Module module = new Hibernate5Module();
    module.enable(Hibernate5Module.Feature.FORCE_LAZY_LOADING);
    return module;
  }

  /**
   * Módulo Jackson para configurar serializadores y deserializadores
   * personalizados
   *
   * @return {@link Module}
   */
  @Bean
  public Module databindModule() {
    SimpleModule module = new SimpleModule();
    module.setMixInAnnotation(TestDTO.class, AbstractDummyJsonMixin.class);
    module.addSerializer(Date.class, new CustomDateSerializer());
    return module;
  }


}
