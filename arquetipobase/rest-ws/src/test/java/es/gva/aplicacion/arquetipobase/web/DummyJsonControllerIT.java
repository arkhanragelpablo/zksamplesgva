/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.gva.aplicacion.arquetipobase.repository.model.HDummy;
import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.model.DummyDTO;


/**
 * Pruebas de integración del controlador JSON
 * 
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TestJsonController.class, excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE)}, secure = false)
@AutoConfigureJsonTesters
@AutoConfigureTestDatabase(replace=Replace.NONE)
@ActiveProfiles("dev")
public class DummyJsonControllerIT {

  @MockBean
  TestService testService;

  /** Mapeos entre objetos Java y JSON */
  @Autowired
  private ObjectMapper mapper;

  /** Identificador que debe tener el primer dummy creado */
  static final Long DUMMY_ID = new Long(1);

  /** Permite ejecutar peticiones web */
  @Autowired
  private MockMvc mvc;

  /**
   * Realiza una petición "POST /api/test" para crear dummy.
   * 
   * <p>
   * Debe devolver un código de estado 201 e incluir el header Location con la
   * URL al nuevo recurso.
   * </p>
   * 
   * @throws Exception Error al crear.
   */
  @Test
  public final void createCorrecto() throws Exception {
    // Preparar

	HDummy dum1 = new HDummy();
	dum1.setNombre("Nombre1");  
	  
	HDummy dum2 = new HDummy();
	dum2.setId(DUMMY_ID);
	dum2.setNombre("Nombre2");  

	when(testService.save(any(HDummy.class))).thenReturn(DummyDTO.toModel(dum2));

	String json = mapper.writeValueAsString(dum1);

	MockHttpServletRequestBuilder peticion =
	     post("/api/test").accept(MediaType.APPLICATION_JSON)
	       .contentType(MediaType.APPLICATION_JSON_VALUE).content(json);

	// Ejecutar
	ResultActions resultado = this.mvc.perform(peticion);

	// Validar
	resultado.andDo(print()).andExpect(status().isCreated()).andExpect(header()
	        .string("Location", "http://localhost/api/test/" + DUMMY_ID));
	
  }




}