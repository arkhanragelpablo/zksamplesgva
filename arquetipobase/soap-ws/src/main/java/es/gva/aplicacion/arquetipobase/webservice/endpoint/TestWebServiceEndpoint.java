/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.webservice.endpoint;

import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

import java.util.ArrayList;
import java.util.List;

import arquetipobase.aplicacion.schemas.testwebservice.TestVO;
import arquetipobase.aplicacion.webservice.testwebservice.TestWebServiceFaultMsg;
import arquetipobase.aplicacion.webservice.testwebservice.TestWebServicePortType;

/**
 * Clase que implementa el servicio web SOAP <i>TestWebService</i>.
 *
 *
 */
public class TestWebServiceEndpoint implements TestWebServicePortType {

  // Servicios con la lógica de negocio necesaria para los servicios web SOAP

  private final TestService testService;

  /**
   * Construye el objeto e inyecta las dependencias.
   * 
   * @param testService Servicio de test.
   */
  public TestWebServiceEndpoint(TestService testService) {
    super();
    this.testService = testService;
  }

  /**
   * {@inheritDoc}
   * 
   * Obtiene todos los test.
   */
  @Override
  public List<TestVO> obtenerTest() throws TestWebServiceFaultMsg {
    List<TestVO> listaTestVO = new ArrayList<TestVO>();
    List<TestDTO> listaTest = testService.testRead();
    for(TestDTO testDTO:listaTest) {
      listaTestVO.add(mapearTestDTOToTestVO(testDTO));
    }
    return listaTestVO;
  }
  
  /**
   * Mapea un objeto {@link MascotoInfo} en un objeto {@link TestVO}.
   * 
   * @param testDTO objeto {@link testDTO} a mapear.
   * @return testVO Objeto resultante.
   */
  private TestVO mapearTestDTOToTestVO(TestDTO testDTO) {
    TestVO testVO = new TestVO();
    testVO.setId(testDTO.getId());
    testVO.setNombre(testDTO.getNombre());
    return testVO;
  }
  
}
