/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
/**
 * Configuraciones de la aplicación.
 * 
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 */
package es.gva.aplicacion.arquetipobase.webservice.config;
