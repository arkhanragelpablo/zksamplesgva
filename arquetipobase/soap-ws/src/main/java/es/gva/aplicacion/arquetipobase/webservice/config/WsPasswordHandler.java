/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.webservice.config;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * Establece la clave de un certificado.
 * 
 * <p>
 * La clave se utiliza para acceder al certificado con el que firmar la
 * respuesta de un servicio web CXF.
 * </p>
 * 
 */
@Component
public class WsPasswordHandler implements CallbackHandler {

  /** Clave del certificado */
  @Value("${signature.password}")
  private String signaturePassword;

  /**
   * Constructor por defecto.
   */
  public WsPasswordHandler() {
    super();
  }

  /**
   * Obtiene la clave de la propiedad y la establece en el callback.
   * 
   * @param callbacks Callbacks.
   */
  @Override
  public void handle(Callback[] callbacks)
      throws IOException, UnsupportedCallbackException {

    for (Callback callback : callbacks) {
      WSPasswordCallback pwdCallback = (WSPasswordCallback) callback;
      int usage = pwdCallback.getUsage();
      if (usage == WSPasswordCallback.SIGNATURE) {
        pwdCallback.setPassword(signaturePassword);
      }
    }
  }

}
