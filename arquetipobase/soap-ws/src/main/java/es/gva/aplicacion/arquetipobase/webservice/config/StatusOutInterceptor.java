/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.webservice.config;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.http.HttpStatus;

/**
 * Establece un codigo de estado HTTP en la respuesta de un servicio web CXF.
 * 
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */
//tag::wsStatusOutInterceptor[]
public class StatusOutInterceptor extends AbstractPhaseInterceptor<Message> {

  /** Código de estado HTTP */
  private final HttpStatus status;

  /**
   * Interceptor de salida con un código de estado HTTP.
   * 
   * @param status Código de estado HTTP.
   */
  public StatusOutInterceptor(HttpStatus status) {
    super(Phase.POST_PROTOCOL);
    this.status = status;
  }

  /**
   * Establece el código HTTP en la respuesta.
   * 
   * @param status Código de estado HTTP.
   */
  @Override
  public void handleMessage(Message message) {
    message.put(Message.RESPONSE_CODE, this.status.value());
  }

}
//end::wsStatusOutInterceptor[]