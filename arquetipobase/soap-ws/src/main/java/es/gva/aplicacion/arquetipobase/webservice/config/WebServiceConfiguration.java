/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.webservice.config;

import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.webservice.endpoint.TestWebServiceEndpoint;

import io.tracee.binding.cxf.TraceeCxfFeature;

import org.apache.cxf.Bus;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.Endpoint;

/**
 * Configuración de servicios web SOAP
 *
 * <p>
 * Clase de configuración para publicar los servicios web SOAP.
 * </p>
 *
 */
// tag::wsConfiguration[]
@Configuration
public class WebServiceConfiguration {

  /** Servicio de logging para mostrar trazas */
  private static final Logger LOGGER =
      LoggerFactory.getLogger(WebServiceConfiguration.class);
  // end::wsConfiguration[]

  // tag::wsRegisterEndpointProperties[]
  /** Bus donde se publican los servicios web SOAP */
  private final Bus bus;

  // Servicios con la lógica de negocio necesaria para los servicios web SOAP

  /** Servicio con la lógica de negocio de las test */
  private final TestService testService;

  /** Ruta bajo la que se publican los servicios web SOAP */
  @Value("${cxf.path:/services}")
  private String cxfServletPath;
  // end::wsRegisterEndpointProperties[]

  // tag::wsEndpointSecurityProperties[]
  /** Fichero con las propiedades de la firma */
  @Value("${signature.file}")
  private String signaturePropFile;

  /** Alias del certificado con el que firmar la respuesta */
  @Value("${signature.alias}")
  private String signatureAlias;

  /** Manejador de la clave del certificado */
  private final WsPasswordHandler wsPasswordHandler;
  // end::wsEndpointSecurityProperties[]

  /** Si el interceptor esta activado */
  @Value("${server.interceptor.activate}")
  private String serverSec;

  /**
   * Construye el objeto e inyecta las dependencias.
   * 
   * @param bus Bus en el que registrar los servicios.
   * @param testService Servicio de test.
   * @param propietarioService Servicio de propietarios.
   * @param wsPasswordHandler Manejador de la clave del certificado.
   */
  // tag::wsEndpointConstructor[]
  public WebServiceConfiguration(Bus bus, TestService testService,
      WsPasswordHandler wsPasswordHandler) {
    super();
    this.bus = bus;
    this.testService = testService;
    this.wsPasswordHandler = wsPasswordHandler;
  }
  // end::wsEndpointConstructor[]

  /**
   * Este método se encarga de registrar el Endpoint del servicio web SOAP de
   * test en el Bus de Apache CXF.
   * 
   * <p>
   * Registra 2 {@link Features} de Apache CXF:
   * <ul>
   * <li>{@link TraceeCxfFeature} obtiene el ID de traza enviado por el cliente
   * y lo muestra en los logs de la petición.</li>
   * <li>{@link LoggingFeature} muestra en los logs los mensajes SOAP recibidos
   * y enviados. Para mostrar los logs el paquete `org.apache.cxf` debe tener
   * verbosidad INFO.</li>
   * </ul>
   * </p>
   *
   * @return Endpoint Endpoint sel servicio publicado.
   */
  // tag::wsRegisterEndpointStart[]
  @Bean
  public Endpoint testWebServiceEndpoint() {

    EndpointImpl endpoint =
        new EndpointImpl(bus, new TestWebServiceEndpoint(testService));
    // end::wsRegisterEndpointStart[]

    // tag::wsEndpointTracee[]
    // Identificación de trazas y escritura de los mensajes SOAP en log
    endpoint.setFeatures(
        Arrays.asList(new TraceeCxfFeature(), new LoggingFeature()));
    // end::wsEndpointTracee[]

    // tag::wsEndpointSecurity[]
    
    // Activamos el interceptor
    if ("true".equals(serverSec)) {
      // Propiedades del interceptor de seguridad de las peticiones recibidas
      Map<String, Object> inProps = new HashMap<>();
      // Realizar la acción de firma
      inProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE);
      // Fichero con las propiedades de verificación de la firma
      inProps.put(WSHandlerConstants.SIG_VER_PROP_FILE, signaturePropFile);


      // Crear el interceptor con las propiedades y asociarlo al endpoint
      endpoint
          .setInInterceptors(Arrays.asList(new WSS4JInInterceptor(inProps)));
      // end::wsEndpointSecurity[]

      // tag::wsEndpointSecurityOut[]
      // Propiedades del interceptor de seguridad de las respuestas
      Map<String, Object> outProps = new HashMap<>();
      // Realizar la acción de firma
      outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE);
      // Alias del certificado a usar con el que firmar el mensaje
      outProps.put(WSHandlerConstants.SIGNATURE_USER, signatureAlias);
      // Manejador de la clave del certificado
      outProps.put(WSHandlerConstants.PW_CALLBACK_REF, wsPasswordHandler);
      // Fichero con las propiedades de la firma
      outProps.put(WSHandlerConstants.SIG_PROP_FILE, signaturePropFile);
      // Indicar que haga referencia por identificador a la firma
      outProps.put(WSHandlerConstants.SIG_KEY_ID, "DirectReference");
      // Generar la firma sin nodos "inclusiveNamespaces"
      outProps.put(WSHandlerConstants.IS_BSP_COMPLIANT, "false");
      // Configurar la cabecera de seguridad con mustUnderstand
      outProps.put(WSHandlerConstants.MUST_UNDERSTAND, "1");
      // Crear el interceptor con las propiedades y asociarlo al endpoint
      endpoint
          .setOutInterceptors(Arrays.asList(new WSS4JOutInterceptor(outProps)));
      // end::wsEndpointSecurityOut[]

      // tag::wsEndpointSecurityFault[]
      // Crear el interceptor de respuestas de error con código de estado HTTP
      // 200
      endpoint.setOutFaultInterceptors(
          Arrays.asList(new StatusOutInterceptor(HttpStatus.OK)));
      // end::wsEndpointSecurityFault[]
    }
    // tag::wsRegisterEndpointEnd[]
    // Publica el servicio en el path de CXF
    endpoint.publish("/TestWebService");
    return endpoint;
  }
  // end::wsRegisterEndpointEnd[]

  /**
   * Registrar el filtro <i>OpenEntityManagerInViewFilter</i> para las
   * operaciones publicadas bajo la URL definida en cxf.path utilizando el
   * componente <i>FilterRegistrationBean</i> provisto por Spring Boot.
   * 
   * <p>
   * Gracias a este filtro estará disponible el JPA EntityManager en el thread
   * de la petición para la obtención de relaciones perezosas (LAZY).
   * </p>
   *
   * @return FilterRegistrationBean Filtro OpenEntityManagerInViewFilter.
   */
  // tag::wsInViewFilter[]
  @Bean
  public FilterRegistrationBean openEntityManagerInViewFilter() {

    FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
    filterRegBean.setFilter(new OpenEntityManagerInViewFilter());
    List<String> urlPatterns = new ArrayList<>();
    urlPatterns.add(cxfServletPath.concat("/*"));
    filterRegBean.setUrlPatterns(urlPatterns);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(
          "Registrando el filtro 'OpenEntityManagerInViewFilter' para la URL "
              .concat(cxfServletPath));
    }

    return filterRegBean;
  }
  // end::wsInViewFilter[]

}
