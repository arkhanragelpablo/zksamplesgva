package es.gva.aplicacion.arquetipobase.vm.modulos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Window;

import es.gva.aplicacion.arquetipobase.vm.BaseVM;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public class ModalEjemplo extends BaseVM {


	  @Getter
	  @Setter
	  ConsultaNombreSDTO solicitudSeleccionada = new ConsultaNombreSDTO();
	  
	  @Getter
	  @Setter
	  private String coincidenciaNombre;
	  
	  @Getter
	  @Setter
	  private String nombreSeleccionado1;
	  
	  @Getter
	  @Setter
	  private String nombreSeleccionado2;
	  
	  @Getter
	  @Setter
	  private String nombreSeleccionado3;
	
	  @Getter
	  @Setter
	  private List<String> estadoSolicitudes = new ArrayList<>();


	  @Init
	  public void init() {
		    
		    solicitudSeleccionada.setCodigoIdioma("ES");
		    solicitudSeleccionada.setConsultaNombreId(34L);
		    solicitudSeleccionada.setConsultaNombreNumeroReserva("ABC-0001");
		    solicitudSeleccionada.setConsultaNombreFechaFinReserva(null);
		    solicitudSeleccionada.setCodigoMunicipio(46L);
		    solicitudSeleccionada.setCodigoProvincia(3L);
		    solicitudSeleccionada.setIdNombreReservado(34L);
		    solicitudSeleccionada.setMunicipio("Algemesi");
		    solicitudSeleccionada.setPersonaTramiteApellido1("Perez");
		    solicitudSeleccionada.setPersonaTramiteApellido2("Perez");
		    solicitudSeleccionada.setPersonaTramiteDocumento("11111111E");
		    solicitudSeleccionada.setPersonaTramiteEmail("federico@email.com");
		    solicitudSeleccionada.setPersonaTramiteNombre("Federico");
		    solicitudSeleccionada.setPersonaTramiteTelefono("66666666");
		    solicitudSeleccionada.setPersonaTramiteTipoDocumento("N");
		    solicitudSeleccionada.setProvincia("Valencia");
		    solicitudSeleccionada.setTipoEntidadCodigo("SD");
		    solicitudSeleccionada.setTipoEntidadDescripcion("Club Deportivo");
		    solicitudSeleccionada.setTramiteId(1L);
		    solicitudSeleccionada.setTramiteRegistroFecha(new Date());
		    solicitudSeleccionada.setTramiteRegistroJustificante(245L);
		    solicitudSeleccionada.setTramiteRegistroNumero("ABC-001");
		    
		    estadoSolicitudes.add("Solicitada");
		    estadoSolicitudes.add("Rechazada");
		    estadoSolicitudes.add("Aceptada");
	  }

	  @Command
	  public void guardarNombres(@ContextParam(ContextType.VIEW) Window comp) {
	    comp.detach();
	  }

	  @Command
	  public void close(@ContextParam(ContextType.VIEW) Window comp) {
	    comp.detach();
	  }


	  @Command
	  @NotifyChange("nombresCoincidentes")
	  public void findCoincidencias() {
	    
	  }
	  
	  private void comprobarCoincidenciaNombre(ConsultaNombreSDTO solicitudSeleccionada) {
	    
	  }
	  
	  @Data
	  public class ConsultaNombreSDTO {
		  
		  private String codigoIdioma;
		  private Date consultaNombreFechaFinReserva;
		  private Long consultaNombreId;
		  private String consultaNombreNumeroReserva;
		  private String personaTramiteApellido1;
		  private String personaTramiteApellido2;
		  private String personaTramiteDocumento;
		  private String personaTramiteEmail;
		  private String personaTramiteNombre;
		  private String personaTramiteTelefono;
		  private String personaTramiteTipoDocumento;
		  private Long codigoMunicipio;
		  private String municipio;
		  private Long idNombreReservado;
		  private Long codigoProvincia;
		  private String provincia;
		  private Date tramiteRegistroFecha;
		  private Long tramiteRegistroJustificante;
		  private String tramiteRegistroNumero;
		  private String tipoEntidadCodigo;
		  private String tipoEntidadDescripcion;
		  private Long tipoEntidadDenominacionId;
		  private Long tramiteId;
		  private Long nombre1Id;
		  private Long nombre2Id;
		  private Long nombre3Id;
		  private String nombre1;
		  private String nombre2;
		  private String nombre3;

		
	  }
	
	
}
