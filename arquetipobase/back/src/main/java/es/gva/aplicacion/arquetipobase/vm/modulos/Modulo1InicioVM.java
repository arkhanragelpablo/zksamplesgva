package es.gva.aplicacion.arquetipobase.vm.modulos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import es.gva.aplicacion.arquetipobase.model.ConstantesWeb;
import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;
import es.gva.aplicacion.arquetipobase.vm.BaseVM;

import org.zkoss.zk.ui.Executions;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

/**
 * Modelo de la vista del modulo 1
 * 
 * <p>
 * Módulo 1
 * </p>
 * 
 * @author indra
 */

public class Modulo1InicioVM extends BaseVM {

  // Inyectamos el servicio de test
  @WireVariable("testServiceImpl")
  private TestService testService;

  @Getter
  @Setter
  private List<TestDTO> listado;

  @Getter
  @Setter
  private TestDTO objeto;

  /**
   * Modulo1
   */
  @Init(superclass = true)
  public void init() {
    listado = testService.testRead();
  }

  /**
   * abrimos una ventana para visualizar los permisos
   * 
   * @param
   * @throws InterruptedException
   */
  @Command("verObjeto")
  public final void verObjeto() {

    final Map<String, Object> params = new HashMap<>();
    params.put("objeto", this.objeto);

    final Window ventana =
        (Window) Executions.createComponents("/modulos/modulo1/mant-objeto.zul",
            this._component, params);
    ventana.doModal();
  }
  
  @Command("verObjetoModal")
  public final void verObjetoModal(@BindingParam(value="objeto") TestDTO objetoRecibido) {
	  
    final Map<String, Object> params = new HashMap<>();
    params.put("objeto", objetoRecibido);
    
    Executions.createComponents("/modulos/modulo1/mant-objeto-modal.zul",
            null , params);
  }
  
  @Command("crearNuevaEntidad")
  public final void crearNuevaEntidad() {
      final Map<String, Object> params = new HashMap<>();
      params.put("objeto", new TestDTO());
      
      Executions.createComponents("/modulos/modulo1/mant-objeto-modal2.zul",
              null , params);
      
  }
  
  @GlobalCommand("addListado")
  @NotifyChange("listado")
  public final void addListado(@BindingParam("testDTO")TestDTO testDTO) {
      listado.add(testDTO);
  }
  
  
}

