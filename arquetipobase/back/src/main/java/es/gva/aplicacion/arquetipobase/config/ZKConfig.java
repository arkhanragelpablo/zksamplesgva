/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;
import org.zkoss.zk.au.http.DHtmlUpdateServlet;
import org.zkoss.zk.ui.http.DHtmlLayoutServlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSessionListener;

/**
 * Configuración de ZK.
 * 
 * <p>
 * Servlets, filters y listeners.
 * </p>
 * 
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */

@Configuration
public class ZKConfig {

  /**
   * Mapea peticiones con las páginas y la URI que gestiona las peticiones AJAX.
   * 
   * <p>
   * Gestiona las peticiones de las páginas con extensión *.zul y configura la
   * URI /zkau para gestionar las peticiones AJAX.
   * </p>
   * 
   * @return Servlet que gestiona las página ZK.
   */
  @Bean
  public ServletRegistrationBean dHtmlLayoutServlet() {
	  
    ServletRegistrationBean dHtmlLayoutServlet = new ServletRegistrationBean(new DHtmlLayoutServlet(), "*.zul");
    dHtmlLayoutServlet.setName("zkLoader");
    dHtmlLayoutServlet.setLoadOnStartup(1);
    Map<String, String> parametros = new HashMap<>();
    parametros.put("update-uri", "/zkau");
    dHtmlLayoutServlet.setInitParameters(parametros);
    return dHtmlLayoutServlet;
  }

  /**
   * Mapea la URI que gestiona las peticiones AJAX.
   * 
   * <p>
   * Las peticiones AJAX son gestionadas en la URI /zkau/* y proporcionan de
   * forma dinámica la vista del lado cliente conforme va siendo necesaria.
   * </p>
   * 
   * @return Servlet que gestina las peticiónes AJAX.
   */
  @Bean
  public ServletRegistrationBean dHtmlUpdateServlet() {
	  
    ServletRegistrationBean dHtmlUpdateServlet = new ServletRegistrationBean(new DHtmlUpdateServlet(), "/zkau/*");
    dHtmlUpdateServlet.setName("auEngine");
    return dHtmlUpdateServlet;
  }

  /**
   * Mantiene la sesión de las entidades en la vista,
   * 
   * <p>
   * Habilita la carga de relaciones uno a muchos perezosas (LAZY).
   * </p>
   * 
   * @return Filtro de sesión desde las entidades de la vista.
   */
  /*
   * bloqueamos el acceso a las sesiones de la capa de datos  
  @Bean
  public OpenEntityManagerInViewFilter openEntityManagerInViewFilter() {
    return new OpenEntityManagerInViewFilter();
  }
  */

  /**
   * Limpia la información de la sesión del lado cliente de la sesión.
   * 
   * @return Listener de sesión.
   */
  @Bean
  public HttpSessionListener httpSessionListener() {
    return new org.zkoss.zk.ui.http.HttpSessionListener();
  }

  /**
   * Permite acceder a la petición y al idioma.
   * 
   * <p>
   * El acceso se realiza desde el hilo que gestiona una petición.
   * </p>
   * 
   * @return Listener de petición.
   */
  @Bean
  public RequestContextListener requestContextListener() {
    return new RequestContextListener();
  }

}
