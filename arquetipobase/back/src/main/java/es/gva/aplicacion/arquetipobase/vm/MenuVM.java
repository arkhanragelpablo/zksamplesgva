package es.gva.aplicacion.arquetipobase.vm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;

/**
 * Modelo de la vista del menú.
 * 
 * <p>
 * Obtiene todas las páginas del servicio de navegación.
 * </p>
 * 
 * @author indra
 */
public class MenuVM {

  List<Pagina> paginas = new ArrayList<Pagina>();

  /**
   * Crea las páginas del menú.
   */
  public MenuVM() {

    super();

    // Crea las páginas de la aplicación y las añade al mapa
    paginas.add(new Pagina(Labels.getLabel("m1.titulo") , "/modulos/modulo1/index.zul"));
    paginas.add(new Pagina(Labels.getLabel("m2.titulo") , "/modulos/modulo2/index.zul"));
    
  }

  /**
   * Obtiene la lista de páginas del menú.
   * 
   * @return Lista de páginas.
   */
  public List<Pagina> getPaginas() {
    return paginas;
  }

  /**
   * Modelo de la vista de una página.
   * 
   * @author indra
   */
  public static class Pagina implements Serializable {

    /** Identificador único de la clase al serializar */
    private static final long serialVersionUID = -7349083600521742918L;

    /** Clave multi idioma con el nombre de la página */
    String etiqueta;

    /** Ruta a la página */
    String src;

    /**
     * Crea una nueva página.
     * 
     * @param etiqueta Descripción.
     * @param src Ruta.
     */
    public Pagina(String etiqueta, String src) {
      this.etiqueta = etiqueta;
      this.src = src;
    }

    /**
     * @return Etiqueta multi idioma de la página.
     */
    public String getEtiqueta() {
      return etiqueta;
    }

    /**
     * @return URL de la página.
     */
    public String getSrc() {
      return src;
    }
  }

}

