/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.security;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

/**
 * Clase para al integración de ZK con Spring Security
 * 
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 */
public class ZkSecurityUtil {

  /**
   * Constructor.
   */
  protected ZkSecurityUtil() {}

  /**
   * Comprueba si el usuario autenticado posee el role especificacido.
   * 
   * @param rol Role.
   * @return true si el usuario autenticado posee el rol especificado.
   */
  public static boolean hasRole(String rol) {
    if (!StringUtils.hasLength(rol)) {
      return false;
    }

    SecurityExpressionRoot securityExpressionRoot = new SecurityExpressionRoot(
        SecurityContextHolder.getContext().getAuthentication()) {};

    return securityExpressionRoot.hasRole(rol);
  }

  /**
   * Comprueba si el usuario autenticado posee alguno de los roles.
   * 
   * @param roles Array de roles.
   * @return true si el usuario autenticado posee alguno de los roles.
   */
  public static boolean hasAnyRole(String[] roles) {
    if (roles.length <= 0) {
      return false;
    }

    SecurityExpressionRoot securityExpressionRoot = new SecurityExpressionRoot(
        SecurityContextHolder.getContext().getAuthentication()) {};

    return securityExpressionRoot.hasAnyRole(roles);
  }

}
