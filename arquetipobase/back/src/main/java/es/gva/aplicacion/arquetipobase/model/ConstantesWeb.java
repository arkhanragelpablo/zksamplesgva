package es.gva.aplicacion.arquetipobase.model;

/**
 * 
 * @author Indra
 */

/**
 * Class ConstantesWeb.
 */
public final class ConstantesWeb {
	
  /**
   * Instancia un nuevo constantes web de ConstantesWeb.
   */
  private ConstantesWeb() { };


  public static final String PANTALLA_BACKGROUND = "/comun/wBackAplicacion.zul";
  
  public static final String SES_ESTILO_DEFECTO = "ESTILO_DEFECTO";
  
  public static final String ZK_ESTILO_DEFECTO = "sapphire";
  
  /**
   * Atributo constante LOCALE_CASTELLANO.
   */
  public static final String LOCALE_CASTELLANO = "es_ES";

  /**
   * Atributo constante LOCALE_VALENCIANO.
   */
  public static final String LOCALE_VALENCIANO = "ca_ES";

  
  /**
   * VACIO.
   */
  public static final String VACIO = "";
  /**
   * ESPACIO.
   */
  public static final String ESPACIO = " ";

  // --- Parametros entre ventanas

  /** Parametro filtro web, que contiene todos los objetos. */
  public static final String PARAMETRO_FILTROWEB = "filtroWeb";

  /** Constante para variables de desktop zk. */
  public static final String VARIABLE_DESKTOP = "ZK_VBLE_DESKTOP_";
  
  /** Valor altura de escritorio. */
  public static final String DESKTOP_HEIGHT = "DesktopHeight";
  /** Valor anchura de escritorio. */
  public static final String DESKTOP_WIDTH = "DesktopWidth";
  /** Valor por defecto de atlura de escritorio. */
  public static final int DESKTOP_HEIGHT_DEFECTO = 570;
  /** Valor por defecto de atlura de pantalla. */
  public static final int SCREEN_HEIGHT_DEFECTO = 664;
  /** Parametro para pasar un textbox. */


  /**
   * Atributo constante MSGWARNING de Constantes.
   */
  public static final String MSGWARNING = "Warning";

  /**
   * Atributo constante MSGCONFIRMACION de Constantes.
   */
  public static final String MSGCONFIRMACION = "Confirmación";

  /**
   * Atributo constante MSGINFO de Constantes.
   */
  public static final String MSGINFO = "Info";

  /**
   * Atributo constante MSGSEGWINDOW de Constantes.
   */
  public static final String MSGSEGWINDOW = "mensaje.seguridad.ventana";

  /**
   * Atributo constante MSGSEGCONTROL de Constantes.
   */
  public static final String MSGSEGCONTROL = "mensaje.seguridad.control";

  /**
   * Atributo constante MSGSEGMENU de Constantes.
   */
  public static final String MSGSEGMENU = "mensaje.seguridad.menu";

  /** Atributo constante IDIOMA_USUARIO de ConstantesWEB. */
  public static final String IDIOMA_USUARIO = "idiomaUsiario";

  public static final String PARAMETRO_REPORT_EJEMPLO = "dato_param";

  public static final String MODAL_USUARIO = "/comun/modalUsuario.zul";

  /** Atributo constante MODAL MENSAJES ConstantesWEB. */
  public static final String MODAL_DESCONECTAR = "/comun/modalDesconectar.zul";
  public static final String MODAL_DESCONECTAR_ICON = "/comun/modalDesconectarIcon.zul";
  public static final String MODAL_WARNING = "/comun/modalWarning.zul";
  public static final String MODAL_SUCCESS = "/comun/modalSuccess.zul";
  public static final String MODAL_INFO = "/comun/modalInfo.zul";
  public static final String MODAL_DANGER = "/comun/modalDanger.zul";
  public static final String MODAL_WARNING_ICON = "/comun/modalWarningIcon.zul";
  
  /** Atributo constante MODAL EJEMPLO FORMULARIO ConstantesWEB. */
  public static final String MODAL_EXAMPLE = "/modulos/modal/modalExample.zul";

  public static final String WINDOW_GRID = "/modulos/grid/chachigrid.zul";

public static final String CABECERA_CENTROS = "/comun/cabeceraCentros.zul";
public static final String CABECERA_DEFAULT = "/comun/cabecera.zul";

public static final String MODAL_CENTROS = "/comun/modalCambioCentro.zul";

  
}