package es.gva.aplicacion.arquetipobase.vm;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

//Para poder inyectar beans de spring
@VariableResolver(DelegatingVariableResolver.class)
public class BaseVM {
		
	 // Variables de contexto de ZK 
	 protected Execution _execution = null;	
	 protected Component _component = null;
	 protected Component _view = null;	
	 protected IdSpace _spaceOwner = null;
	 protected Page _page = null;
	 protected Desktop _desktop = null;
	 protected Session _session = null;
	 protected WebApp _application = null;
	 protected BindContext _bindContext = null;
	 protected Binder _binder = null;
	 
	 // Variables generales de aplicacion
	 protected Locale _locale = null;
	 
	// consultas paginadas y filtro
     protected int pageSize = 10;
     protected long totalSize = 0;
     protected int activePage = 0;
     protected Map<String, Object> filtro = new HashMap<String, Object>();
	
	 // Inicializamos las variables de contexto de ZK que podemos utilizar en todos los vista-modelo
	 @Init
	 public void init( @ContextParam(ContextType.EXECUTION) Execution execution,
		        @ContextParam(ContextType.COMPONENT) Component component,
		        @ContextParam(ContextType.VIEW) Component view,
		        @ContextParam(ContextType.SPACE_OWNER) IdSpace spaceOwner, 
		        @ContextParam(ContextType.PAGE) Page page,
		        @ContextParam(ContextType.DESKTOP) Desktop desktop,
		        @ContextParam(ContextType.SESSION) Session session,
		        @ContextParam(ContextType.APPLICATION) WebApp application,	 
		        @ContextParam(ContextType.BIND_CONTEXT) BindContext bindContext,
		        @ContextParam(ContextType.BINDER) Binder binder
		   ) {
		 
		 this._execution = execution;
		 this._component = component;
		 this._view = view;
		 this._spaceOwner = spaceOwner;
		 this._page = page;
		 this._desktop = desktop;
		 this._session = session;
		 this._application = application;
		 this._bindContext = bindContext;
		 this._binder = binder;				 
		 this._locale = (Locale) _session.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE);
		 
	 }

	 public int getPageSize() {
       return pageSize;
   }

   
   public void setPageSize(int pageSize) {
       this.pageSize = pageSize;
   }

   public long getTotalSize() {
       return totalSize;
   }

   
   public void setTotalSize(long totalSize) {
       this.totalSize = totalSize;
   }

   public int getActivePage() {
       return activePage;
   }

   public void setActivePage(int activePage) {
       this.activePage = activePage;
   }

   public Map<String, Object> getFiltro() {
       return filtro;
   }

   public void setFiltro(Map<String, Object> filtro) {
       this.filtro = filtro;
   }
}
