package es.gva.aplicacion.arquetipobase.vm.modulos;

import es.gva.aplicacion.arquetipobase.model.ConstantesWeb;
import es.gva.aplicacion.arquetipobase.service.api.EjemploChachiService;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.ChachiGridDTO;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.ColumnaDTO;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.ContextoBusquedaDTO;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.LineaListaDTO;
import es.gva.aplicacion.arquetipobase.vm.BaseVM;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zk.ui.event.SortEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.theme.Themes;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Controlador pagina principal.
 */
public class ChachiGridVM extends BaseVM {

	private static final Logger log = LoggerFactory.getLogger(ChachiGridVM.class);
	private static final String ESPANYOL = "es_ES";
	private final String VALENCIANO = "ca_ES";

	private String username;
	private String perfilStr;
	private String ultimoAccesoStr;
	private String eMail;
	private Boolean esidiomaVal;  // idioma seleccionado valenciano

	 /**
	 * Atributo que almacena el valor de la ruta de la pantalla de primer nivel
	 */
	private String srcContenidoL1;
	
	private List<LineaListaDTO> listaDatos = new ArrayList<LineaListaDTO>();

	@WireVariable("ejemploChachiServiceImpl")
	private EjemploChachiService ejemploChachiService;

	
	private String sortField="campoA";
	private boolean sortDirectionASC = true;
	private String filterField = "";
	private boolean count = false;
	ChachiGridDTO respuesta = null;
	private Listheader lh = null;
	@Init(superclass = true)
	public void init() {

		// esta info debería venir del contexto de seguridad de spring, al menos en nausica
		username = "Nombre Apellido1 Apellido2";
		perfilStr = "(Administrador)";
		ultimoAccesoStr = "22/05/2018 12:34";
		eMail = "user1@gva.es";		

		
		
		// estilo por defecto
		if (_session.getAttribute(ConstantesWeb.SES_ESTILO_DEFECTO)==null)
			_session.setAttribute(ConstantesWeb.SES_ESTILO_DEFECTO, "Iceblue");
		
		Themes.setTheme(Executions.getCurrent(), (String)_session.getAttribute("Iceblue"));
		
		// idioma por defecto,  deberia venir de nausica
		if (_locale==null) {
			changeIdioma("es_ES");
		}
		
		esidiomaVal = (_locale.toString()).equals(VALENCIANO); 
		
		// pantalla por defecto
		srcContenidoL1 = ConstantesWeb.PANTALLA_BACKGROUND;

		
		/*
		ContextoBusquedaDTO contextoBusqueda = new ContextoBusquedaDTO();
		contextoBusqueda.setItemStartNumber(getActivePage());
		contextoBusqueda.setPageSize(getPageSize());
		respuesta = ejemploChachiService.obtieneListaDatos(contextoBusqueda);
		listaDatos = respuesta.getDatos();
		 */
		listaDatos = new ArrayList<LineaListaDTO>();
		
		count = false;
		setActivePage(0);
		refreshData();
		
	}



	
	/**
	 * Metodo para cambiar el idioma
	 * 
	 * @param evento
	 *            Parámetro e
	 */
	@Command("changeIdioma")
	public final void changeIdioma(final @BindingParam("idi") String idioma) {

		Locale preferLocale = new Locale(idioma);
		if (idioma.length() > 2) {
			preferLocale = new Locale(idioma.substring(0, 2), idioma.substring(3));
		}

		_session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, preferLocale);	
		_locale = (Locale)_session.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE);
		
		//this.usuarioLogado.setIdioma(idiomaPlataforma);		
		
		esidiomaVal = (_locale.toString()).equals(VALENCIANO); 
		_execution.sendRedirect("");	

	}

	/**
	 * Cambia el estilo por defecto de zk
	 */
	@Command("cambiaEstilo")
	public void cambiaEstilo (final @BindingParam("estilo") String e) {

	  	_session.setAttribute(ConstantesWeb.SES_ESTILO_DEFECTO, e);
	  	// cambiamos de tema
  		Themes.setTheme(Executions.getCurrent(), (String)_session.getAttribute(ConstantesWeb.SES_ESTILO_DEFECTO));
  		Executions.sendRedirect("");
	  		
	}


	@Command
	@NotifyChange({"listaDatos","activePage"})
	public void onPaging(@ContextParam(ContextType.TRIGGER_EVENT) PagingEvent event) {	    
	  setActivePage(event.getActivePage());        
	  refreshData();
    }
   
	
	@Command
	@NotifyChange({"listaDatos","totalSize"})	
    public void onSort(@ContextParam(ContextType.TRIGGER_EVENT) SortEvent event) {

	    sortField = ((Listheader)event.getTarget()).getId();
	    sortDirectionASC = event.isAscending();
	    refreshData();            
	}
	   
	
	
	@Command
	@NotifyChange({"listaDatos","totalSize","activePage"})
	public void search(){
	  count = false;
	  setActivePage(0);
	  refreshData();
	  
	}
	
	
	   
    @Command
    @NotifyChange({"listaDatos","totalSize"})
    public void contar(@ContextParam(ContextType.TRIGGER_EVENT) MouseEvent event){     
      count = (!count);
      if(lh.getLabel().contains("*c")) {
        lh.setLabel(lh.getLabel().substring(0, lh.getLabel().length()-2));
      }else {
        lh.setLabel(lh.getLabel() + "*c");
      }
      refreshData();
      
    }
    
    @Command
    public void onRightClickColumn(@ContextParam(ContextType.TRIGGER_EVENT) MouseEvent event) {
      
      lh = (Listheader)event.getTarget();            
    }
	
	private void refreshData(){

      ContextoBusquedaDTO contextoBusqueda = new ContextoBusquedaDTO();      
      List<ColumnaDTO> columnas = new ArrayList<ColumnaDTO>();
      ColumnaDTO col = new ColumnaDTO();
      col.setNombre(sortField);
      col.setAscendente(sortDirectionASC);
      if(filterField!=null && filterField.trim().length()>0) {
        col.setFiltro(filterField);
      }
     
      col.setAgrupado(count);
      
      columnas.add(col);
      
      contextoBusqueda.setColumnas(columnas);
      
      contextoBusqueda.setItemStartNumber(getActivePage());
      contextoBusqueda.setPageSize(getPageSize());
      
      respuesta = ejemploChachiService.obtieneListaDatos(contextoBusqueda);
      listaDatos = respuesta.getDatos();   
      setTotalSize(respuesta.getTotalSize());
	  
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUltimoAccesoStr() {
		return ultimoAccesoStr;
	}

	public void setUltimoAccesoStr(String ultimoAccesoStr) {
		this.ultimoAccesoStr = ultimoAccesoStr;
	}


	public Boolean getEsidiomaVal() {
		return esidiomaVal;
	}

	public void setEsidiomaVal(Boolean esidiomaVal) {
		this.esidiomaVal = esidiomaVal;
	}

	public String getSrcContenidoL1() {
		return srcContenidoL1;
	}

	public void setSrcContenidoL1(String srcContenidoL1) {
		this.srcContenidoL1 = srcContenidoL1;
	}

	public String getPerfilStr() {
		return perfilStr;
	}

	public void setPerfilStr(String perfilStr) {
		this.perfilStr = perfilStr;
	}


	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String geteMail() {
		return eMail;
	}

  public List<LineaListaDTO> getListaDatos() {
    return listaDatos;
  }

  public void setListaDatos(List<LineaListaDTO> listaDatos) {
    this.listaDatos = listaDatos;
  }

  public String getSortField() {
    return sortField;
  }

  public void setSortField(String sortField) {
    this.sortField = sortField;
  }

  public boolean getSortDirectionASC() {
    return sortDirectionASC;
  }

  public void setSortDirectionASC(boolean directionASC) {
    this.sortDirectionASC = directionASC;
  }

  public String getFilterField() {
    return filterField;
  }

  public void setFilterField(String filterField) {
    this.filterField = filterField;
  }

  
  
  
}
