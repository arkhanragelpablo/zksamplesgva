package es.gva.aplicacion.arquetipobase.vm;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;
import org.zkoss.zul.theme.Themes;

import es.gva.aplicacion.arquetipobase.model.ConstantesWeb;
import lombok.Getter;
import lombok.Setter;


/**
 * Controlador pagina principal.
 */
public class PaginaPrincipalVM extends BaseVM {

	private static final Logger log = LoggerFactory.getLogger(PaginaPrincipalVM.class);
	private static final String ESPANYOL = "es_ES";
	private final String VALENCIANO = "ca_ES";

	private String username;
	private String perfilStr;
	private String ultimoAccesoStr;
	private String eMail;
	private Boolean esidiomaVal;  // idioma seleccionado valenciano

	 /**
	 * Atributo que almacena el valor de la ruta de la pantalla de primer nivel
	 */
	private String srcContenidoL1;
	@Getter @Setter
	private String srcCabecera;


	@Init(superclass = true)
	public void init() {

		// esta info debería venir del contexto de seguridad de spring, al menos en nausica
		username = "Nombre Apellido1 Apellido2";
		perfilStr = "(Administrador)";
		ultimoAccesoStr = "22/05/2018 12:34";
		eMail = "user1@gva.es";		

		// estilo por defecto
		if (_session.getAttribute(ConstantesWeb.SES_ESTILO_DEFECTO)==null)
			_session.setAttribute(ConstantesWeb.SES_ESTILO_DEFECTO, ConstantesWeb.ZK_ESTILO_DEFECTO);
		
		// idioma por defecto,  deberia venir de nausica
		if (_locale==null) {
			changeIdioma("es_ES");
		}
		
		esidiomaVal = (_locale.toString()).equals(VALENCIANO); 
		
		// pantalla por defecto
		srcCabecera = ConstantesWeb.CABECERA_DEFAULT;
		srcContenidoL1 = "/modulos/modulo1/index.zul";
		
		
	}

	@Command("menuTramites")
	@NotifyChange({ "srcContenidoL1", "visibleL1" })
	public final void menuTramites() throws InterruptedException {
		srcContenidoL1 = "/modulos/modulo1/index.zul";
	}

	@Command("menuMisSolicitudes")
	@NotifyChange({ "srcContenidoL1", "visibleL1" })
	public final void menuMisSolicitudes() throws InterruptedException {
		srcContenidoL1 = "/modulos/modulo2/index.zul";
	}

	@Command("menuInformes")
	@NotifyChange({ "srcContenidoL1" })
	public final void menuInformes() {
		srcContenidoL1 = "/modulos/informes/informes.zul";
	}
	
	@Command("showGrid")
	@NotifyChange({ "srcContenidoL1" })
	public final void showGrid() {
		srcContenidoL1 = ConstantesWeb.WINDOW_GRID;
	}
	
	@Command("cabeceraCentros")
	@NotifyChange({ "srcCabecera" })
	public final void cabeceraCentros() throws InterruptedException {
		if(srcCabecera.equals(ConstantesWeb.CABECERA_DEFAULT)) {
			srcCabecera = ConstantesWeb.CABECERA_CENTROS;
		} else {
			srcCabecera = ConstantesWeb.CABECERA_DEFAULT;
			
		}
	}

	/**
	 * Método para abrir versión de la aplicación
	 * 
	 */
	@Command("menuVersion")
	@NotifyChange({ "srcContenidoL1", "visibleL1" })
	public final void menuVersion() throws InterruptedException {
		srcContenidoL1 = "/comun/wVersion.zul";
	}
	
	/**
	 * Realiza logout.
	 */
	@Command("salir")
	public final void salir() {
		/*
		mostrarMessageBox(Labels.getLabel("logout.mensaje"), Labels.getLabel("mensaje.atencion"),
				Messagebox.YES + Messagebox.NO, Messagebox.EXCLAMATION, new EventListener<Event>() {
					public final void onEvent(final Event event) {
						if (((Integer) event.getData()).intValue() == Messagebox.YES) {

							// anulamos la sesion
							_session.setAttribute(Constantes.USER_NAUSICA, null);
							_session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, null);
							SecurityUtils.logout();
							String logOutURL = SSOAddonConfigurationHolder.getConfiguration().getSsoLogOutURL().toString();
							logOutURL+="?callbackUrl="+((HttpServletRequest)Executions.getCurrent().getNativeRequest()).getRequestURL();
						    Executions.sendRedirect(logOutURL);
						    						    
						}
					}
				});
				
		*/		
	}

	
	/**
	 * Metodo para cambiar el idioma
	 * 
	 * @param evento
	 *            Parámetro e
	 */
	@Command("changeIdioma")
	public final void changeIdioma(final @BindingParam("idi") String idioma) {

		Locale preferLocale = new Locale(idioma);
		if (idioma.length() > 2) {
			preferLocale = new Locale(idioma.substring(0, 2), idioma.substring(3));
		}

		_session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, preferLocale);	
		_locale = (Locale)_session.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE);
		
		//this.usuarioLogado.setIdioma(idiomaPlataforma);		
		
		esidiomaVal = (_locale.toString()).equals(VALENCIANO); 

		_execution.sendRedirect("");	
	}

	/**
	 * Cambia el estilo por defecto de zk
	 */
	@Command("cambiaEstilo")
	public void cambiaEstilo (final @BindingParam("estilo") String e) {

	  	_session.setAttribute(ConstantesWeb.SES_ESTILO_DEFECTO, e);
	  	// cambiamos de tema
  		Themes.setTheme(Executions.getCurrent(), (String)_session.getAttribute(ConstantesWeb.SES_ESTILO_DEFECTO));
  		Executions.sendRedirect("");
	  		
	}
	
	@Command
	public void showUser() {
		Executions.createComponents(ConstantesWeb.MODAL_USUARIO, null, null);
	}
	
	@Command
	public void showModalExample() {
		Executions.createComponents(ConstantesWeb.MODAL_EXAMPLE, null, null);
	}
	
	@Command
	public void showModalCentros() {
		Executions.createComponents(ConstantesWeb.MODAL_CENTROS, null, null);
	}
	
	@Command("mostrarModal")
	public void desconectar(@BindingParam("tipoModal") String tipoModal) {
		switch (tipoModal) {
		case "success": 
			Executions.createComponents(ConstantesWeb.MODAL_SUCCESS, null, null);
			break;
		case "info": 
			Executions.createComponents(ConstantesWeb.MODAL_INFO, null, null);
			break;
		case "warning": 
			Executions.createComponents(ConstantesWeb.MODAL_WARNING_ICON, null, null);
			break;
		case "danger": 
			Executions.createComponents(ConstantesWeb.MODAL_DANGER, null, null);
			break;
		case "desconectar": 
			Executions.createComponents(ConstantesWeb.MODAL_DESCONECTAR_ICON, null, null);
			break;
		default:
			Executions.createComponents(ConstantesWeb.MODAL_INFO, null, null);
			break;
		}
	}
	
	@Command("close")
	public void close(@ContextParam(ContextType.VIEW) Window comp) {
		comp.detach();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUltimoAccesoStr() {
		return ultimoAccesoStr;
	}

	public void setUltimoAccesoStr(String ultimoAccesoStr) {
		this.ultimoAccesoStr = ultimoAccesoStr;
	}


	public Boolean getEsidiomaVal() {
		return esidiomaVal;
	}

	public void setEsidiomaVal(Boolean esidiomaVal) {
		this.esidiomaVal = esidiomaVal;
	}

	public String getSrcContenidoL1() {
		return srcContenidoL1;
	}

	public void setSrcContenidoL1(String srcContenidoL1) {
		this.srcContenidoL1 = srcContenidoL1;
	}

	public String getPerfilStr() {
		return perfilStr;
	}

	public void setPerfilStr(String perfilStr) {
		this.perfilStr = perfilStr;
	}


	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String geteMail() {
		return eMail;
	}
	

}
