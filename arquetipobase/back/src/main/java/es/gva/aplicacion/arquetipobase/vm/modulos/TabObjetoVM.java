package es.gva.aplicacion.arquetipobase.vm.modulos;

import es.gva.aplicacion.arquetipobase.vm.BaseVM;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;

import lombok.Getter;
import lombok.Setter;

public class TabObjetoVM extends BaseVM {

  @Getter
  @Setter
  private String texto;

  @Init(superclass = true)
  public void init(@ExecutionArgParam("tostring") String parametro) {

    this.texto = parametro;

  }
}
