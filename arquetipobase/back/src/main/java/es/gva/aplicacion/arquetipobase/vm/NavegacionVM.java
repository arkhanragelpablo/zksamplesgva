/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.vm;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.theme.Themes;

/**
 * Modelo de la vista de la navegación.
 * 
 * <p>
 * Permite navegar en modo AJAX-Based entre distintas páginas.
 * </p>
 * 
 * @author mmartinez at http://www.disid.com[DISID Corporation S.L.]
 */
public class NavegacionVM {

  /** Ruta de la página a mostrar por defecto */
  private String src;

  /** Estilo */
  private String estilo;

  
  /** Uso de cookies no aceptado **/
  private boolean cookie;

  /**
   * Constructor por defecto.
   */
  public NavegacionVM() {
    super();
    this.src = "/modulos/modulo1/index.zul";
  }

  /**
   * Al iniciar la vista, comprueba si se ha aceptado el uso de cookies.
   */
  @Init
  public void init() {

    // Comprobar cookie
    cookie = false;
    HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
    if (request == null || request.getCookies() == null) {
      return;
    }
      
    for (Cookie currentCookie : request.getCookies()) {
      if ("cookie".equals(currentCookie.getName())) {
        this.cookie = true;
        break;
      }
    }
  }

  /**
   * Añade la cookie de aceptación del uso de cookies.
   */
  @GlobalCommand
  @NotifyChange({ "src"})
  public void navegar(final @BindingParam("pagina") String p) {
	  
	  	this.src = p;

  		if (Themes.getCurrentTheme().equals("sapphire")) {
  		  	// cambiamos de tema
  	  		Themes.setTheme(Executions.getCurrent(), "atlantic");
  	  		Executions.sendRedirect("");
  		}
  		else {
  		  	// cambiamos de tema
  	  		Themes.setTheme(Executions.getCurrent(), "sapphire");
  	  		Executions.sendRedirect("");
  		}
  		
  }

  /**
   * Añade la cookie de aceptación del uso de cookies.
   */
  @Command
  public void cambiaEstilo (final @BindingParam("estilo") String e) {
	  
	  	this.estilo = e;

  		if (Themes.getCurrentTheme().equals("sapphire")) {
  		  	// cambiamos de tema
  	  		Themes.setTheme(Executions.getCurrent(), "atlantic");
  	  		Executions.sendRedirect("");
  		}
  		else {
  		  	// cambiamos de tema
  	  		Themes.setTheme(Executions.getCurrent(), "sapphire");
  	  		Executions.sendRedirect("");
  		}
  		
  }

  
  
  /**
   * Añade la cookie de aceptación del uso de cookies.
   */
  @GlobalCommand
  @NotifyChange("cookie")
  public void cookie() {

    // Añadir cookie
    HttpServletResponse response = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
    response.addCookie(new Cookie("cookie", ""));
    this.cookie = true;
  }

  /**
   * @return URL de la navegación.
   */
  public String getSrc() {
    return src;
  }

  /**
   * @return Uso de cookies ?
   */
  public boolean getCookie() {
    return cookie;
  }

  /**
   * @param Uso de cookies ?
   */
  public void setCookie(boolean cookie) {
    this.cookie = cookie;
  }

	public String getEstilo() {
		return estilo;
	}
	
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

}
