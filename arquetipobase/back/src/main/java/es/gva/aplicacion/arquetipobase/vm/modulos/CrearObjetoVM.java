package es.gva.aplicacion.arquetipobase.vm.modulos;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;
import es.gva.aplicacion.arquetipobase.vm.BaseVM;
import lombok.Getter;
import lombok.Setter;

public class CrearObjetoVM extends BaseVM {

    @Getter
    @Setter
    private TestDTO objeto;

    @WireVariable("testServiceImpl")
    private TestService testService;

    @Init(superclass = true)
    public void init(@ExecutionArgParam("objeto") final TestDTO parametro) {

        this.objeto = parametro;

    }

    @Command("create")
    public final void create(@BindingParam("cmp") final Window self) {
        final TestDTO testDTO = this.testService.create(this.objeto);

        final Map<String, Object> args = new HashMap<String, Object>();
        args.put("testDTO", testDTO);
        BindUtils.postGlobalCommand(null, null, "addListado", args);
        self.detach();
    }



}
