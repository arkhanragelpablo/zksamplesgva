package es.gva.aplicacion.arquetipobase.vm.modulos;

import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;
import es.gva.aplicacion.arquetipobase.vm.BaseVM;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import lombok.Getter;
import lombok.Setter;

public class DetalleObjetoVM extends BaseVM {

  @Getter
  @Setter
  private TestDTO objeto;

  @Init(superclass = true)
  public void init(@ExecutionArgParam("objeto") TestDTO parametro) {

    this.objeto = parametro;

  }
}
