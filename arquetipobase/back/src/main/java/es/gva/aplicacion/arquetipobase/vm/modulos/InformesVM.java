package es.gva.aplicacion.arquetipobase.vm.modulos;


import es.gva.aplicacion.arquetipobase.model.ConstantesWeb;
import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;
import es.gva.aplicacion.arquetipobase.service.api.report.ReportService;
import es.gva.aplicacion.arquetipobase.vm.BaseVM;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Modelo de la vista de la ventana informes
 * 
 * @author obeses
 */
public class InformesVM extends BaseVM {

  @WireVariable("reportServiceImpl")
  private ReportService servicioReport;
  
  @WireVariable("testServiceImpl")
  private TestService servicioTest;
  
  private Map<String, Object> filtros;
  private List<TestDTO> misDatos;  

  @Init(superclass = true)
  public void init() {
    filtros = new HashMap<>();
    misDatos = servicioTest.testRead();
  }
  
  /**
   * Imprime un informe de ejemplo en PDF con datos de años academicos
   */
  @Command
  public void exportarEjemploPDF() {
    List<?> datos = misDatos;
    filtros.put(ConstantesWeb.IDIOMA_USUARIO, _locale.toString());
    filtros.put(ConstantesWeb.PARAMETRO_REPORT_EJEMPLO, 12345);
    servicioReport.generarReport("EjemploPDF","ResultadoPDF","PDF",filtros,datos);
  }

  /**
   * Imprime un informe de ejemplo en XLS con datos de años academicos
   */
  @Command
  public void exportarEjemploXLS() {
    List<?> datos = misDatos;
    filtros.put(ConstantesWeb.IDIOMA_USUARIO, _locale.toString());
    filtros.put(ConstantesWeb.PARAMETRO_REPORT_EJEMPLO, 12345);
    servicioReport.generarReport("EjemploHojaCalc","ResultadoXLS","XLS",filtros,datos);
  }
  
  /**
   * Imprime un informe de ejemplo en ODS con datos de años academicos
   */
  @Command
  public void exportarEjemploODS() {
    List<?> datos = misDatos;
    filtros.put(ConstantesWeb.IDIOMA_USUARIO, _locale.toString());
    filtros.put(ConstantesWeb.PARAMETRO_REPORT_EJEMPLO, 12345);
    servicioReport.generarReport("EjemploHojaCalc","ResultadoODS","ODS",filtros,datos);
  }
  
}

