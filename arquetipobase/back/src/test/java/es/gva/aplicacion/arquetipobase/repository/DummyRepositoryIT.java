/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.repository;

import static org.assertj.core.api.Assertions.assertThat;

import es.gva.aplicacion.arquetipobase.repository.springdata.TestRepository;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Pruebas de integración del repositorio {@link DummyRepository}
 *
 * <p>
 * Cada método de pruebas debe anotarse de forma obligatoria con la anotación
 * `@Test` ya que permite reconocer los métodos de la clase que ejecutan
 * pruebas.
 * <p>
 *
 * <p>
 * Se importan de forma estática los métodos de la clase _Assert_ y de la clase
 * _Assertions_ para que las aserciones de validación sean más concisas.
 * </p>
 * 
 * @author jrcasanya at http://www.disid.com[DISID Corporation S.L.]
 */

@Ignore
@RunWith(SpringRunner.class)
@DataJpaTest
public class DummyRepositoryIT {

  @Autowired
  TestRepository testRepository;

  @Autowired
  TestEntityManager testEntityManager;

  TestDTO dum;

  /** Visitas persistidas en base de datos */
  List<TestDTO> listaDummyPersistidas;

  /**
   * Fase de preparación
   *
   * <p>
   * Antes de la ejecución de todas las pruebas se ejecuta este método que
   * inicializa la base de datos
   * <p>
   *
   * <p>
   * Crear en base de datos:
   * <ul>
   * <li>1 Ciudad para poder insertar propietarios</li>
   * <li>1 Propietario para poder insertar mascotas</li>
   * <li>1 Mascotas para poder insertar visitas</li>
   * <li>3 Visitas con gecha varias</li>
   * </ul>
   * </p>
   */
  @Before
  public void preparar() {

    TestDTO dum1 = new TestDTO();
    dum1.setNombre("Nombre1");

    TestDTO dum2 = new TestDTO();
    dum2.setNombre("Nombre2");
    
    testEntityManager.persist(dum1);
    testEntityManager.persist(dum2);

  }

  /**
   * Validar el comportamiento del método
   * {@link TestRepositoryImpl#find}.
   *
   * <p>
   * Se valida que el número de resultados es el correcto y que los resultados
   * son correctos
   * </p>
   * 
   * <p>
   * Las visitas añadidas en el test se añaden al listado de visitas para que
   * puedan ser eliminadas de la base de datos en la fase de limpieza
   * </p>
   */
  @Test
  public final void findDummys() {

    // Verificar: se ha devuelto el número de resultados correcto
    assertThat(listaDummyPersistidas.size()).as("El número de dummys no es correcto")
        .isEqualTo(2);

    // Verificar: se ha devuelto el resutado correcto
    assertThat(listaDummyPersistidas.get(0).getNombre())
        .as("El dummy obtenido no es correcto").isEqualTo("Nombre1");
  }

  /**
   * Fase de limpieza
   *
   * <p>
   * Después de la ejecución de todas las pruebas se ejecuta este método que
   * limpia la base de datos
   * <p>
   * 
   * <p>
   * Elimina las vistas creadas en los test. Los test deben guardar en el
   * listado de vistas, aquellas visitas que inserten en la base de datos
   * </p>
   */
  @After
  public void limpiar() {
	  
    if (listaDummyPersistidas != null && !listaDummyPersistidas.isEmpty()) {
    	listaDummyPersistidas.forEach((dummy) -> testEntityManager.remove(dummy));
    }
    
  }


}
