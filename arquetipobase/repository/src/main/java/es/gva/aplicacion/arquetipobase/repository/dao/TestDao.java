/**
 * TestDao.java 6 abr. 2020
 *
 * Copyright 2020 INDITEX.
 * Departamento de Sistemas
 */
package es.gva.aplicacion.arquetipobase.repository.dao;

import java.util.List;

import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

/**
 * Test DAO (con Spring Data).
 *
 * @author <a href="pablogra@ext.inditex.com">Pablo Guti&eacute;rrez Ragel</a>
 */
public interface TestDao {

    /**
     * Guarda objeto.
     *
     * @param dum
     *            objeto a guardar
     * @return id objeto
     */
    Long update(final TestDTO dum);

    /**
     * Obtiene todos los objetos.
     * @return objetos
     */
    List<TestDTO> getAll();

    /**
     * Obtiene objeto.
     *
     * @param id objeto
     *
     * @return objeto
     */
    TestDTO findById(Long id);

    /**
     * Crea el.
     *
     * @param name name
     * @return the test DTO
     */
    TestDTO create(String name);

    /**
     * Crea el.
     *
     * @param testDTO test DTO
     * @return the test DTO
     */
    TestDTO create(TestDTO testDTO);

}