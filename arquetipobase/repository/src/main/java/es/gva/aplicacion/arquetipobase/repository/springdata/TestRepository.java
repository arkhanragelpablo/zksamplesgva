package es.gva.aplicacion.arquetipobase.repository.springdata;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import es.gva.aplicacion.arquetipobase.repository.model.HTest;
import es.gva.aplicacion.arquetipobase.repository.model.HTestInfo;

/**
 * Repositorio Spring Data JPA: operaciones interfaz gestionadas por SpringData + operaciones específicas definidas en TestSpringDataRepositoryCustom.
 */
@Transactional(readOnly = true)
public interface TestRepository extends TestRepositoryCustom, CrudRepository<HTest, Long> {

    /**
     * Ejemplo de consulta con la notación <i>@Query</i> de Spring Data JPA
     *
     * <p>
     * Devuelve {@link HTest}s cuyo last name empieza por el parámetro
     * proporcionado.
     * </p>
     *
     * @see <a href=
     *      "http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query">
     *      Using @Query</a>
     *
     * @param apellidos Valor por el que buscar
     * @return una lista de {@link HTest}s (o una lista vacía si no se
     *         encuentra nada)
     */
    @Query("SELECT DISTINCT dum FROM HTest dum WHERE dum.nombre LIKE :nombre%")
    List<HTest> findByNombre(@Param("nombre") String nom);

    /**
     * Ejemplo de consulta con la notación <i>@Query</i> de Spring Data JPA
     *
     * <p>
     * Recupera un objeto de tipo {@link HTest} por su id
     * </p>
     *
     * @see <a href=
     *      "http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query">
     *      Using @Query</a>
     *
     * @param id the id to search for
     * @return the {@link HTest} if found
     *
     */
    @Query("SELECT dum FROM HTest dum WHERE dum.id =:id")
    HTest findById(@Param("id") Long id);

    /**
     * Ejemplo de consulta nativa con la notación <i>@Query</i> de Spring Data JPA
     *
     * <p>
     * Calcula el número de HDummy
     * </p>
     *
     * @see <a href=
     *      "http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query">
     *      Using @Query</a>
     *
     * @return número de dum
     *
     */
    @Query(
            value = "SELECT COUNT(DISTINCT dum.ID) dum FROM HTest dum",
            nativeQuery = true)
    long countByTipoId();

    /**
     * Ejemplo de proyección a partir de nombre de método
     *
     * <p>
     * Realiza la proyección del listado de todos los dum sobre un listado de
     * DTO's del tipo {@link HTestInfo}
     * </p>
     *
     * @return
     */
    List<HTestInfo> findAllProjectedBy();

    /**
     * Ejemplo de proyección y paginación a partir de nombre de método
     *
     * <p>
     * Realiza la proyección del listado de todos los HDummy sobre un listado de
     * DTO's del tipo {@link HTestInfo} y pagina los resultados.
     * </p>
     *
     * @param pageable
     * @return
     */
    Page<HTestInfo> findAllProjectedBy(Pageable pageable);

}
