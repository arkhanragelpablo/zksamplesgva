package es.gva.aplicacion.arquetipobase.repository.springdata;

import org.springframework.transaction.annotation.Transactional;

import es.gva.aplicacion.arquetipobase.repository.model.HTest;

/**
 * Interfaz para consultas dinámicas del repositorio de la entidad
 * {@link HTest}
 *
 * <p>
 * Añade los métodos con consultas dinámicas para el repositorio de la entidad
 * {@link HTest}.
 *
 * Si se quiere crear un método con consultas dinámicas se deber añadir en esta
 * interfaz y su implementación añadirla en {@link TestRepositoryImpl}.
 * </p>
 *
 * Para más información ver:
 * <ul>
 * <li><a href=
 * "http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories"/>Repositories</a>
 * </li>
 * </ul>
 *
 * @author cordin at http://www.disid.com[DISID Corporation S.L.]
 *
 */
@Transactional(readOnly = true)
public interface TestRepositoryCustom {

  /**
   * Desconecta un HDummy del sistema de persistencia.
   * 
   * @param HDummy.
   */
  void detach(HTest propietario);

}
