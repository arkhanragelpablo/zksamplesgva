package es.gva.aplicacion.arquetipobase.repository.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.gva.aplicacion.arquetipobase.repository.model.HTest;
import es.gva.aplicacion.arquetipobase.repository.springdata.TestRepository;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

/**
 * Implementación DAO usando repositorios Spring Data.
 * @author rsanz
 *
 */
@Repository
public class TestDaoImpl implements TestDao {

    private static long counter = 1L;

    @Autowired
    private TestRepository testRepository;

    @Override
    public Long update(final TestDTO dum) {
        final HTest oh = HTest.fromModel(dum);
        this.testRepository.save(oh);
        return oh.getId();
    }

    @Override
    public List<TestDTO> getAll() {
        final List<HTest> hRes = (List<HTest>) this.testRepository.findAll();
        return HTest.toModel(hRes);
    }


    @Override
    public TestDTO findById(final Long id) {
        final HTest oh = this.testRepository.findById(id);
        return HTest.toModel(oh);
    }

    @Override
    public TestDTO create(final String nombre) {
        final HTest newHTests = new HTest();
        newHTests.setNombre(nombre);
        return HTest.toModel(this.testRepository.save(newHTests));
    }

    @Override
    public TestDTO create(final TestDTO testDTO) {
        return HTest.toModel(this.testRepository.save(HTest.fromModel(testDTO)));
    }

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomAlphaNumeric(int count) {
        final StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            final int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }


}
