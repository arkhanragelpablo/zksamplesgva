/**
 * Copyright (c) 2017 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.gva.aplicacion.arquetipobase.repository.model;

/**
 * Data Transfer Object <i>HDummyInfo</i>.
 *
 * <p>
 * Datos básicos de un HDummy
 * </p>
 *
 * Para más información ver:
 * <ul>
 * <li><a href=
 * "http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#projections">Projections</a>
 * </li>
 * </ul>
 *
 * @author jcgarcia at http://www.disid.com[DISID Corporation S.L.]
 *
 */
public interface HTestInfo {

  /** Identificador del propietario */
  Long getId();

  /** Nombre del propietario */
  String getNombre();


}
