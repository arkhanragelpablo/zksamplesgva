package es.gva.aplicacion.arquetipobase.repository.dao;

import java.util.List;

import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

/**
 * Test DAO (sin Spring Data).
 */
public interface Test2Dao {

	/**
	 * Guarda objeto.
	 *
	 * @param dum
	 *            objeto a guardar
	 * @return id objeto
	 */
	Long update(final TestDTO dum);

	/**
	 * Obtiene todos los objetos.
	 * @return objetos
	 */
	List<TestDTO> getAll();
	
	/**
	 * Obtiene objeto.
	 *
	 * @param id objeto
	 *            
	 * @return objeto
	 */
	TestDTO findById(Long id);
	
	
}