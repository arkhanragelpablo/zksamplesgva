package es.gva.aplicacion.arquetipobase.repository.springdata;

import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.transaction.annotation.Transactional;

import es.gva.aplicacion.arquetipobase.repository.model.HTest;


/**
 * Implementación de consultas específicas.
 */
@Transactional(readOnly = true)
public class TestRepositoryImpl extends QueryDslRepositorySupport
    implements TestRepositoryCustom {
	
  /**
   * Constructor de la clase PropietarioRepositoryImpl
   */
  public TestRepositoryImpl() {
    super(HTest.class);
  }

  @Override
  public void detach(HTest dummy) {
    if (dummy != null) {
      getEntityManager().detach(dummy);
    }
  }

}
