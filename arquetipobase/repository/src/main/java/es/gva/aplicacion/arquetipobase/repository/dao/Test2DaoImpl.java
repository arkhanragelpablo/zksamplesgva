package es.gva.aplicacion.arquetipobase.repository.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import es.gva.aplicacion.arquetipobase.repository.model.HTest;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

/**
 * Implementación DAO usando repositorios Spring Data.
 * @author rsanz
 *
 */
@Repository
public class Test2DaoImpl implements Test2Dao {
	
	/** EntityManager. */
    @PersistenceContext
    private EntityManager entityManager;

	@Override
	public Long update(TestDTO dum) {
		HTest oh = HTest.fromModel(dum);
		entityManager.merge(oh);		
		return oh.getId();
	}

	@Override
	public List<TestDTO> getAll() {
		final Query query = entityManager.createQuery(
                "Select p From HTest p");
        final List<HTest> hRes = query.getResultList();
		return HTest.toModel(hRes);
	}

	@Override
	public TestDTO findById(Long id) {
		HTest oh = entityManager.find(HTest.class, id);
		return HTest.toModel(oh);
	}
	
}
