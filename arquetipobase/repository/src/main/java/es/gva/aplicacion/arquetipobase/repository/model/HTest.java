package es.gva.aplicacion.arquetipobase.repository.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

/**
 * Entidad <i>HDummy</i>
 *
 * <p>
 * Entidad que representa un dummy.
 * </p>
 *
 * @author autor
 *
 */

@Entity
@Table(name = "TEST")
public class HTest implements Serializable {

  private static final long serialVersionUID = -622398920617085054L;

  /** Identificador */
  @Id
  @SequenceGenerator(name = "testGen", sequenceName = "SEQ_TEST", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "testGen")
  @Column(name = "ID")
  private Long id;

  @Column(name = "NOMBRE")
  @NotEmpty
  @Size(max = 30)
  private String nombre;

  /**
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return this.nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }


  /**
   * Dos entidades son iguales si son la misma instancia o tienen el mismo
   * identificador.
   *
   * <p>
   * Esta implementación de `equals` es específica para entidades JPA y usa el
   * identificador de la entidad para ello. Para más información ver:
   * <ul>
   * <li><a href=
   * "https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/"/>
   * How to implement equals and hashCode using the JPA entity identifier
   * (primary key)</a></li>
   * </ul>
   * </p>
   *
   * @param obj referencia a entidad con la que comparar
   * @return true si esta entidad es igual que la entidad recibida como
   *         parámetro; false en caso contrario.
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    return getId() != null && Objects.equals(getId(), ((HTest) obj).getId());
  }

  /**
   * Esta implementación de `hashCode` es específica para entidades JPA y usa un
   * valor fijo de` int` para poder Identificar la entidad en las colecciones
   * después de asignar un nuevo id a la entidad.
   *
   * <p>
   * Para más información ver:
   * <ul>
   * <li><a href=
   * "https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/"/>
   * How to implement equals and hashCode using the JPA entity identifier
   * (primary key)</a></li>
   * </ul>
   * </p>
   *
   * @result código hash
   */
  @Override
  public int hashCode() {
    return 31;
  }

  /**
   * Obtener una representación concisa en modo texto de la entidad.
   * <p>
   * Esta representación es interna y su objetivo es que sea utilizada por los
   * desarrolladores. No utilizar esta representación para usuarios.
   * </p>
   * <p>
   * <b>NUNCA</b> mostrar relaciones, podría penalizar considerablemente el
   * rendimiento de la aplicación. <b>ÚNICAMENTE</b> mostrar atributos de la
   * entidad.
   * </p>
   */
  @Override
  public String toString() {
    return "HDummy {" + "id='" + id + '\'' + ", nombre='" + nombre + "} "
        + super.toString();
  }
  
  /**
   * Convierte a objeto de modelo DTO.
   * 
   * @param oh
   *          hibernate
   * @return objeto modelo
   */
  public static TestDTO toModel(HTest oh) {
	 TestDTO om = null;
	 if (oh != null) {
		 om = new TestDTO();	 
		 om.setId(oh.getId());
		 om.setNombre(oh.getNombre());
	 }
     return om;    
  }   
  
  public static List<TestDTO> toModel(List<HTest> dum) {
	  List<TestDTO> res = null;
	  if (dum != null) {
		  res =  new ArrayList<>();
		  for (HTest oh : dum) {
			  res.add(toModel(oh)); 
		  }
	  }
	  return res;    
  }   
    
  public static HTest fromModel(final TestDTO om) {
		final HTest oh = new HTest();
		if (om != null) {
			oh.setId(om.getId());
			oh.setNombre(om.getNombre());
		}
		return oh;
	}

}
