package es.gva.aplicacion.arquetipobase.service.api.model.comun;

import java.util.Comparator;

public class LineaListaDTO implements Comparable<LineaListaDTO>{

  String campoA;
  String campoB;
  String campoC;
  String campoD;
  String campoE;
  String campoF;
  private int contador;
  
  public LineaListaDTO() {
    
  }

  public LineaListaDTO(String campoA, String campoB, String campoC,
      String campoD, String campoE,String campoF, int contador) {
    super();
    this.campoA = campoA;
    this.campoB = campoB;
    this.campoC = campoC;
    this.campoD = campoD;
    this.campoE = campoE;
    this.campoF = campoF;
    this.contador = contador;
  }

  /**
   * @return the campoA
   */
  public String getCampoA() {
    return campoA;
  }

  /**
   * @param campoA the campoA to set
   */
  public void setCampoA(String campoA) {
    this.campoA = campoA;
  }

  /**
   * @return the campoB
   */
  public String getCampoB() {
    return campoB;
  }

  /**
   * @param campoB the campoB to set
   */
  public void setCampoB(String campoB) {
    this.campoB = campoB;
  }

  /**
   * @return the campoC
   */
  public String getCampoC() {
    return campoC;
  }

  /**
   * @param campoC the campoC to set
   */
  public void setCampoC(String campoC) {
    this.campoC = campoC;
  }

  /**
   * @return the campoD
   */
  public String getCampoD() {
    return campoD;
  }

  /**
   * @param campoD the campoD to set
   */
  public void setCampoD(String campoD) {
    this.campoD = campoD;
  }

  /**
   * @return the campoE
   */
  public String getCampoE() {
    return campoE;
  }

  /**
   * @param campoE the campoE to set
   */
  public void setCampoE(String campoE) {
    this.campoE = campoE;
  }

  /**
   * @return the campoF
   */
  public String getCampoF() {
    return campoF;
  }

  /**
   * @param campoF the campoF to set
   */
  public void setCampoF(String campoF) {
    this.campoF = campoF;
  }
  
  /**
   * @return the contador
   */
  public int getContador() {
    return contador;
  }

  /**
   * @param contador the contador to set
   */
  public void setContador(int contador) {
    this.contador = contador;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((campoA == null) ? 0 : campoA.hashCode());
    result = prime * result + ((campoB == null) ? 0 : campoB.hashCode());
    result = prime * result + ((campoC == null) ? 0 : campoC.hashCode());
    result = prime * result + ((campoD == null) ? 0 : campoD.hashCode());
    result = prime * result + ((campoE == null) ? 0 : campoE.hashCode());
    result = prime * result + ((campoF == null) ? 0 : campoF.hashCode());
    result = prime * result + contador;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    LineaListaDTO other = (LineaListaDTO) obj;
    if (campoA == null) {
      if (other.campoA != null)
        return false;
    } else if (!campoA.equals(other.campoA))
      return false;
    if (campoB == null) {
      if (other.campoB != null)
        return false;
    } else if (!campoB.equals(other.campoB))
      return false;
    if (campoC == null) {
      if (other.campoC != null)
        return false;
    } else if (!campoC.equals(other.campoC))
      return false;
    if (campoD == null) {
      if (other.campoD != null)
        return false;
    } else if (!campoD.equals(other.campoD))
      return false;
    if (campoE == null) {
      if (other.campoE != null)
        return false;
    } else if (!campoE.equals(other.campoE))
      return false;
    if (campoF == null) {
      if (other.campoF != null)
        return false;
    } else if (!campoF.equals(other.campoF))
      return false;    
    if (contador != other.contador)
      return false;
    return true;
  }
  
  @Override
  public int compareTo(LineaListaDTO arg0) {
    return Comparators.CampoA.compare(this, arg0);
  }
  
  
  
  public static class Comparators {

    public static Comparator<LineaListaDTO> CampoA = new Comparator<LineaListaDTO>() {
        @Override
        public int compare(LineaListaDTO o1, LineaListaDTO o2) {
            return o1.campoA.compareTo(o2.campoA);
        }
    };
    public static Comparator<LineaListaDTO> CampoB = new Comparator<LineaListaDTO>() {
      @Override
      public int compare(LineaListaDTO o1, LineaListaDTO o2) {
          return o1.campoB.compareTo(o2.campoB);
      }
    };
    public static Comparator<LineaListaDTO> CampoC = new Comparator<LineaListaDTO>() {
      @Override
      public int compare(LineaListaDTO o1, LineaListaDTO o2) {
          return o1.campoC.compareTo(o2.campoC);
      }
    };
    public static Comparator<LineaListaDTO> CampoD = new Comparator<LineaListaDTO>() {
      @Override
      public int compare(LineaListaDTO o1, LineaListaDTO o2) {
          return o1.campoD.compareTo(o2.campoD);
      }
    };
    public static Comparator<LineaListaDTO> CampoE = new Comparator<LineaListaDTO>() {
      @Override
      public int compare(LineaListaDTO o1, LineaListaDTO o2) {
          return o1.campoE.compareTo(o2.campoE);
      }
    };
    public static Comparator<LineaListaDTO> CampoF = new Comparator<LineaListaDTO>() {
      @Override
      public int compare(LineaListaDTO o1, LineaListaDTO o2) {
          return o1.campoF.compareTo(o2.campoF);
      }
    };    
  }
  
}
