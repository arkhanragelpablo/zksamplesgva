package es.gva.aplicacion.arquetipobase.service.api.exception;

import java.util.Date;

import es.gva.aplicacion.arquetipobase.service.api.model.comun.ListaPropiedades;

/**
 *
 * Contiene datos adicionales de una excepcion de servicio.
 *
 */
public final class ServiceExceptionData {

  /**
   * Detalles de la excepción.
   */
  private ListaPropiedades detallesExcepcion;

  /**
   * Fecha de la excepción.
   */
  private Date fechaExcepcion = new Date();

  /**
   * Obtiene los detalles de la excepción.
   *
   * @return Devuelbe detalles Excepcion
   */
  public ListaPropiedades getDetallesExcepcion() {
    return detallesExcepcion;
  }

  /**
   * Metodo de acceso a fechaExcepcion.
   *
   * @return fechaExcepcion
   */
  public Date getFechaExcepcion() {
    return fechaExcepcion;
  }

  /**
   * Metodo para establecer fechaExcepcion.
   *
   * @param pFechaExcepcion
   *          fechaExcepcion a establecer
   */
  public void setFechaExcepcion(final Date pFechaExcepcion) {
    fechaExcepcion = pFechaExcepcion;
  }

  /**
   * Mótodo para establecer detallesExcepcion.
   *
   * @param pDetallesExcepcion
   *          detallesExcepcion a establecer
   */
  public void setDetallesExcepcion(final ListaPropiedades pDetallesExcepcion) {
    detallesExcepcion = pDetallesExcepcion;
  }

}
