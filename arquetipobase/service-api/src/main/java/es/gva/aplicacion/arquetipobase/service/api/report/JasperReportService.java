package es.gva.aplicacion.arquetipobase.service.api.report;

import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

public interface JasperReportService {

  void compilarJasperReport(String report) throws JRException;

  byte[] generaPDF(String reportName, Map<String, Object> reportParams,
      Collection<?> coleccionDatos, String lang) throws JRException;

  byte[] generaXLS(String reportName, Map<String, Object> reportParams,
      Collection<?> coleccionDatos, String lang) throws JRException;

  byte[] generaODT(String reportName, Map<String, Object> reportParams,
      Collection<?> coleccionDatos, String lang) throws JRException;

  byte[] generaODS(String reportName, Map<String, Object> reportParams,
      Collection<?> coleccionDatos, String lang) throws JRException;

  byte[] generaCSV(String reportName, Map<String, Object> reportParams,
      Collection<?> coleccionDatos, String lang) throws JRException;



}
