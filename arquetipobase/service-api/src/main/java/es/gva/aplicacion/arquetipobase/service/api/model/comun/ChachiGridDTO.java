package es.gva.aplicacion.arquetipobase.service.api.model.comun;

import java.util.List;

public class ChachiGridDTO {

  private int totalSize;
  private List<LineaListaDTO> datos;
  
  public ChachiGridDTO() {
    
  }

  public ChachiGridDTO(int totalSize, List<LineaListaDTO> datos) {
    super();
    this.totalSize = totalSize;
    this.datos = datos;
  }

  /**
   * @return the totalSize
   */
  public int getTotalSize() {
    return totalSize;
  }

  /**
   * @param totalSize the totalSize to set
   */
  public void setTotalSize(int totalSize) {
    this.totalSize = totalSize;
  }

  /**
   * @return the datos
   */
  public List<LineaListaDTO> getDatos() {
    return datos;
  }

  /**
   * @param datos the datos to set
   */
  public void setDatos(List<LineaListaDTO> datos) {
    this.datos = datos;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((datos == null) ? 0 : datos.hashCode());
    result = prime * result + totalSize;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ChachiGridDTO other = (ChachiGridDTO) obj;
    if (datos == null) {
      if (other.datos != null)
        return false;
    } else if (!datos.equals(other.datos))
      return false;
    if (totalSize != other.totalSize)
      return false;
    return true;
  }
}
