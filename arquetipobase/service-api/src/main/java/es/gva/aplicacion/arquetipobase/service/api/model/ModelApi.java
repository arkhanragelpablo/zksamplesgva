package es.gva.aplicacion.arquetipobase.service.api.model;

import java.io.Serializable;

/**
 * Clase de la que heredan todos los objetos del modelo de API.
 *
 * @author Indra
 *
 */
public abstract class ModelApi implements Serializable {

	/** Serial version UID. **/
	private static final long serialVersionUID = 1L;

}
