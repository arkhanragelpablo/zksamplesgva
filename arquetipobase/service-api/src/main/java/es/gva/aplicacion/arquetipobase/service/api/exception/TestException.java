package es.gva.aplicacion.arquetipobase.service.api.exception;

@SuppressWarnings("serial")
public class TestException extends ServiceRollbackException{

	public TestException(String pmessageSRE, Throwable pcauseSRE) {
		super(pmessageSRE, pcauseSRE);
	}


}
