package es.gva.aplicacion.arquetipobase.service.api.model.comun;

public class ColumnaDTO {
 
  private String nombre;
  private String filtro;
  private boolean agrupado;
  private boolean ascendente;
  private boolean orderBy;
  
  public ColumnaDTO() {
    
  }

  public ColumnaDTO(String pNombre, String pFiltro, boolean pAgrupado) {
    super();
    this.nombre = pNombre;
    this.filtro = pFiltro;
    this.agrupado = pAgrupado;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the filtro
   */
  public String getFiltro() {
    return filtro;
  }

  /**
   * @param filtro the filtro to set
   */
  public void setFiltro(String filtro) {
    this.filtro = filtro;
  }

  /**
   * @return the agrupado
   */
  public boolean isAgrupado() {
    return agrupado;
  }

  /**
   * @param agrupado the agrupado to set
   */
  public void setAgrupado(boolean agrupado) {
    this.agrupado = agrupado;
  }

  
  /**
   * @return the ascendente
   */
  public boolean isAscendente() {
    return ascendente;
  }

  /**
   * @param ascendente the ascendente to set
   */
  public void setAscendente(boolean ascendente) {
    this.ascendente = ascendente;
  }

  /**
   * @return the orderBy
   */
  public boolean isOrderBy() {
    return orderBy;
  }

  /**
   * @param orderBy the orderBy to set
   */
  public void setOrderBy(boolean orderBy) {
    this.orderBy = orderBy;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (agrupado ? 1231 : 1237);
    result = prime * result + ((filtro == null) ? 0 : filtro.hashCode());
    result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ColumnaDTO other = (ColumnaDTO) obj;
    if (agrupado != other.agrupado)
      return false;
    if (filtro == null) {
      if (other.filtro != null)
        return false;
    } else if (!filtro.equals(other.filtro))
      return false;
    if (nombre == null) {
      if (other.nombre != null)
        return false;
    } else if (!nombre.equals(other.nombre))
      return false;
    return true;
  }
}
