package es.gva.aplicacion.arquetipobase.service.api.model;

/**
 * Entidad <i>DummyDTO</i>
 *
 * <p>
 * Entidad de Dummy DTO
 * </p>
 *
 * @author INDRA
 *
 */

public class TestDTO extends ModelApi {

  /** Serial version UID. **/
  private static final long serialVersionUID = 1L;

  /** Id. */
  private Long id;

  /** Nombre. */
  private String nombre;

  /** Nombre. */
  private String toString;

  public TestDTO() {
    super();
  }

  public TestDTO(Long id, String nombre) {
    super();
    this.id = id;
    this.nombre = nombre;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getToString() {
    return this.toString();
  }

  public void setToString(String toString) {
    this.toString = toString;
  }

  @Override
  public String toString() {
    return "Test [id=" + id + ", nombre=" + nombre + "]";
  }

}
