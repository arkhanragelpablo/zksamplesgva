package es.gva.aplicacion.arquetipobase.service.api.report;

import java.util.List;
import java.util.Map;

/**
 * Interface ReportService.
 * 
 * @author INDRA
 */
public interface ReportService {

  boolean generarReport(String nomFichJRXML, String nomFichRespuesta,
      String formato, Map<String, Object> filtros, List<?> datos);

}
