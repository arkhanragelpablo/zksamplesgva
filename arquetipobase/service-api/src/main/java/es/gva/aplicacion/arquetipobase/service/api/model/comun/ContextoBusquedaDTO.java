package es.gva.aplicacion.arquetipobase.service.api.model.comun;

import java.util.List;

public class ContextoBusquedaDTO {

  private int itemStartNumber;
  private int pageSize;
  private List<ColumnaDTO> columnas;

  public ContextoBusquedaDTO() {

  }

  public ContextoBusquedaDTO(int pItemStartNumber, int pPageSize, List<ColumnaDTO> pColumnas) {
    super();

    this.itemStartNumber = pItemStartNumber;
    this.pageSize = pPageSize;
    this.columnas = pColumnas;
  }

  /**
   * @return the itemStartNumber
   */
  public int getItemStartNumber() {
    return itemStartNumber;
  }

  /**
   * @param itemStartNumber the itemStartNumber to set
   */
  public void setItemStartNumber(int itemStartNumber) {
    this.itemStartNumber = itemStartNumber;
  }

  /**
   * @return the pageSize
   */
  public int getPageSize() {
    return pageSize;
  }

  /**
   * @param pageSize the pageSize to set
   */
  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  /**
   * @return the columnas
   */
  public List<ColumnaDTO> getColumnas() {
    return columnas;
  }

  /**
   * @param columnas the columnas to set
   */
  public void setColumnas(List<ColumnaDTO> columnas) {
    this.columnas = columnas;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((columnas == null) ? 0 : columnas.hashCode());
    result = prime * result + itemStartNumber;
    result = prime * result + pageSize;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ContextoBusquedaDTO other = (ContextoBusquedaDTO) obj;
    if (columnas == null) {
      if (other.columnas != null)
        return false;
    } else if (!columnas.equals(other.columnas))
      return false;
    if (itemStartNumber != other.itemStartNumber)
      return false;
    if (pageSize != other.pageSize)
      return false;
    return true;
  }

  
}
