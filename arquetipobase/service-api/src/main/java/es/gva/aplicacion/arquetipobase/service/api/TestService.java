/**
 * TestService.java 6 abr. 2020
 *
 * Copyright 2020 INDITEX.
 * Departamento de Sistemas
 */
package es.gva.aplicacion.arquetipobase.service.api;

import java.util.List;

import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;

/**
 *
 * Test Service.
 *
 * @author Indra
 *
 */
public interface TestService {

    /**
     * Test operacion de modificación de datos (debe estar marcada con @Transactional).
     * Si nombre vacío genera una excepción de test para verificar interceptor negocio.
     *
     * @param dum objeto a modificar
     * @return id objeto
     */
    Long testSave(TestDTO dum);


    /**
     * Test operación lectura (marcado por transacción lectura service).
     *
     * @return lista de todas las instancias almacenadas.
     */
    List<TestDTO> testRead();



    /**
     * Crea el.
     *
     * @return the test DTO
     */
    TestDTO create(TestDTO testDTO);

}
