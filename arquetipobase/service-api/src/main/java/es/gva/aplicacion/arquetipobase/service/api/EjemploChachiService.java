package es.gva.aplicacion.arquetipobase.service.api;

import es.gva.aplicacion.arquetipobase.service.api.model.comun.ChachiGridDTO;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.ContextoBusquedaDTO;

public interface EjemploChachiService {

  ChachiGridDTO obtieneListaDatos(ContextoBusquedaDTO contextoBusqueda);
}
