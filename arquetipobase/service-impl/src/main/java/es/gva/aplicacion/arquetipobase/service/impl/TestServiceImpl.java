package es.gva.aplicacion.arquetipobase.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.gva.aplicacion.arquetipobase.repository.dao.Test2Dao;
import es.gva.aplicacion.arquetipobase.repository.dao.TestDao;
import es.gva.aplicacion.arquetipobase.service.api.TestService;
import es.gva.aplicacion.arquetipobase.service.api.model.TestDTO;
import es.gva.aplicacion.arquetipobase.service.impl.component.TestComponent;
import es.gva.aplicacion.arquetipobase.service.impl.interceptor.NegocioInterceptor;

// TODO Pendiente replantear test con BD memoria

/**
 * Implementación de {@link TestService}
 *
 * @author Indra
 */

@Service
@Transactional(readOnly = false)
public class TestServiceImpl implements TestService {

    /**
     * Log del interceptor.
     */
    private final Logger log = LoggerFactory.getLogger(TestServiceImpl.class);


    /** Dao (con spring data) */
    private final TestDao testDao;

    /** Dao (sin spring data) */
    private final Test2Dao test2Dao;

    /** Component */
    private final TestComponent testComponent;

    @PostConstruct
    public void insertTest() {
        this.testDao.create(randomAlphaNumeric(6));
        this.testDao.create(randomAlphaNumeric(9));
        this.testDao.create(randomAlphaNumeric(3));
        this.testDao.create(randomAlphaNumeric(5));

    }

    /**
     * Constructor de la clase e inyección de dependencias.
     *
     * @param repository dao.
     */
    @Autowired
    public TestServiceImpl(final TestDao dao, final Test2Dao dao2, final TestComponent component) {
        this.testDao = dao;
        this.test2Dao = dao2;
        this.testComponent = component;
    }

    /** {@inheritDoc} */
    @Override
    @NegocioInterceptor
    @Transactional
    public Long testSave(final TestDTO dum) {

        this.testComponent.test();

        if (dum.getNombre() ==null) {
            //throw new TestException("Nombre nulo");
        }

        final Long id = this.testDao.update(dum);

        final TestDTO t = this.test2Dao.findById(id);
        this.log.debug(t.toString());

        return id;
    }

    /** {@inheritDoc} */
    @Override
    @NegocioInterceptor
    public List<TestDTO> testRead() {

        this.testComponent.test();

        final List<TestDTO> res = this.testDao.getAll();
        return res;
    }

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomAlphaNumeric(int count) {
        final StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            final int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    @Override
    public TestDTO create(final TestDTO testDTO) {
        return this.testDao.create(testDTO);
    }

}
