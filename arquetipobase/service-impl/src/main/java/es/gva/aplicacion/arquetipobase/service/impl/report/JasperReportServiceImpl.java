package es.gva.aplicacion.arquetipobase.service.impl.report;

import es.gva.aplicacion.arquetipobase.service.api.report.JasperReportService;
import es.gva.aplicacion.arquetipobase.service.impl.interceptor.NegocioInterceptor;
import es.gva.aplicacion.arquetipobase.service.impl.model.Utilidades;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOdsExporterConfiguration;
import net.sf.jasperreports.export.SimpleOdtExporterConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsExporterConfiguration;

/**
 * JasperReportServiceImpl.
 * 
 * @author dsirgado
 * @author obeses
 */
@Service
public class JasperReportServiceImpl
    implements JasperReportService, ServletContextAware {

  private ServletContext servletContext;
  /** Ruta real de los reports */
  private String rutaReports;
  private static final Logger LOG = LoggerFactory.getLogger(JasperReportServiceImpl.class);

  /** Metodo sobreescrito necesario para obtener el la ruta real de los reports */
  @Override
  public void setServletContext(ServletContext servletContext) {
    this.servletContext = servletContext;
  }
  
  @PostConstruct
  public final void init() {
    if (Utilidades.isNotNull(servletContext)) {
      rutaReports = servletContext.getRealPath("/WEB-INF/reports/");
      compilarReports();
    }
  }
  
  /**
   * Recorre la carpeta de los reports y compila todos los reports con extensión jrxml
   * para que no tengan que compilarse en cada petición 
   * (Si se van a pasar ya compilados hay que cargarse esto)
   */
  private void compilarReports() {
    File carpetaReports = new File(rutaReports);
    if (carpetaReports.exists() && carpetaReports.isDirectory()) {
      for (File report : carpetaReports.listFiles()) {
        try {
          String nombreReport = report.getName();
          if (report.isFile() && nombreReport.endsWith(".jrxml")) {
            nombreReport = nombreReport.substring(0, nombreReport.length() - 6);
            compilarJasperReport(nombreReport);
          }
        } catch (JRException e) {
          LOG.error("No se ha podido compilar el report " + report.getName()
              + System.getProperty("line.separator") + e.getMessage());
        }
      }
    } else {
      LOG.warn("No se ha encontrado el directorio de informes esperado: reports");
    }
  }

  /**
   * Método para generar el .jasper 
   * 
   * @param reportName
   * @throws JRException
   */
  public void compilarJasperReport(final String reportName) throws JRException {
    String jrxmlPath = rutaReports + reportName + ".jrxml";
    String jasperPath = getJasperPath(reportName);
    
    String logLine = "Compilando " + jrxmlPath + " en " + jasperPath;
    LOG.info(logLine);

    JasperCompileManager.compileReportToFile(jrxmlPath, jasperPath);
  }


  @NegocioInterceptor
  private ExporterInput generarExporterInput(final String reportName,
      final Map<String, Object> reportParams,
      final Collection<?> coleccionDatos, String lang) throws JRException {

    Locale idioma = new Locale(Utilidades.isNull(lang) ? "es_ES" : lang);
    reportParams.put(JRParameter.REPORT_LOCALE, idioma);

    JRDataSource dataSource = Utilidades.isValidCollection(coleccionDatos)
        ? new JRBeanCollectionDataSource(coleccionDatos)
        : new JREmptyDataSource();

    JasperPrint jPrint = JasperFillManager.fillReport(getJasperPath(reportName),
        reportParams, dataSource);

    return new SimpleExporterInput(jPrint);
  }


  @NegocioInterceptor
  @Override
  public final byte[] generaPDF(final String reportName,
      final Map<String, Object> reportParams,
      final Collection<?> coleccionDatos, final String lang)
      throws JRException {

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ExporterInput exporterInput = generarExporterInput(reportName,reportParams,coleccionDatos,lang);
    
    JRPdfExporter exporter = new JRPdfExporter();  
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
    exporter.setExporterInput(exporterInput);
    exporter.setConfiguration(new SimplePdfExporterConfiguration());
    exporter.exportReport();
    
    return baos.toByteArray();
  }

  @NegocioInterceptor
  @Override
  public final byte[] generaXLS(final String reportName,
      final Map<String, Object> reportParams,
      final Collection<?> coleccionDatos, final String lang)
      throws JRException {

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ExporterInput exporterInput = generarExporterInput(reportName,reportParams,coleccionDatos,lang);
    
    JRXlsExporter exporter = new JRXlsExporter();    
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
    exporter.setExporterInput(exporterInput);
    exporter.setConfiguration(new SimpleXlsExporterConfiguration());
    exporter.exportReport();
    
    return baos.toByteArray();
  }

  @NegocioInterceptor
  @Override
  public final byte[] generaODT(final String reportName,
      final Map<String, Object> reportParams,
      final Collection<?> coleccionDatos, final String lang)
      throws JRException {
    
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ExporterInput exporterInput = generarExporterInput(reportName,reportParams,coleccionDatos,lang);
    
    JROdtExporter exporter = new JROdtExporter();    
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
    exporter.setExporterInput(exporterInput);
    exporter.setConfiguration(new SimpleOdtExporterConfiguration());
    exporter.exportReport();

    return baos.toByteArray();
  }

  /**
   * Genera ODS, es posible que no se pueda abrir con libreoffice puesto que
   * a partir de la versión 5.3.6 de libreoffice hay un error que no abre ficheros .ods
   * generados con jasper. La versión 5.3.6 si que va, pero posteriores a esta no, al menos
   * hasta que lo arreglen.
   *  
   * */
  @NegocioInterceptor
  @Override
  public final byte[] generaODS(final String reportName,
      final Map<String, Object> reportParams,
      final Collection<?> coleccionDatos, final String lang)
      throws JRException {

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ExporterInput exporterInput = generarExporterInput(reportName,reportParams,coleccionDatos,lang);
    
    JROdsExporter exporter = new JROdsExporter();    
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
    exporter.setExporterInput(exporterInput);
    exporter.setConfiguration(new SimpleOdsExporterConfiguration());
    exporter.exportReport();

    return baos.toByteArray();
  }

  @NegocioInterceptor
  @Override
  public final byte[] generaCSV(final String reportName,
      final Map<String, Object> reportParams,
      final Collection<?> coleccionDatos, final String lang)
      throws JRException {

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ExporterInput exporterInput = generarExporterInput(reportName,reportParams,coleccionDatos,lang);
    
    JRCsvExporter exporter = new JRCsvExporter();    
    exporter.setExporterOutput(new SimpleWriterExporterOutput(baos));
    exporter.setExporterInput(exporterInput);
    exporter.setConfiguration(new SimpleCsvExporterConfiguration());
    exporter.exportReport();

    return baos.toByteArray();
  }

  private String getJasperPath(final String reportName) {
    return rutaReports + reportName + ".jasper";
  }
  
}
