package es.gva.aplicacion.arquetipobase.service.impl.model;

/**
 * 
 * @author Indra
 */

/**
 * Class Constantes.
 */
public final class Constantes {

  public static final String PARAMETRO_REPORT_EJEMPLO = "dato_param";
  public static final String ESPACIO = " ";
  public static final String VACIO = "";

}
