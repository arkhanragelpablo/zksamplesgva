package es.gva.aplicacion.arquetipobase.service.impl.report;

import es.gva.aplicacion.arquetipobase.service.api.report.ReportService;
import es.gva.aplicacion.arquetipobase.service.impl.model.Utilidades;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.zul.Filedownload;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

/**
 * class ReportServiceImpl.
 *
 * @author indra
 */
@Service
@Transactional
public class ReportServiceImpl extends JasperReportServiceImpl
    implements ReportService {

  private final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);

  /**
   * Método para Generar archivo de la clase ReportServiceImpl.
   *
   * @param nomFichJRXML : El nombre el report sin la extensión .jrxml
   * @param nomFichSalida : Nombre que tendrá el fichero resultante (sin extensión)
   * @param formato PDF, XLS, ODS o CSV 
   * @param filtros : Parámetro filtros
   * @param datos : Parámetro datos
   * 
   * @return devuelve true si no ha habido errores y false si han habido
   *
   */
  @Override
  public boolean generarReport(String nomFichJRXML, String nomFichSalida,
      String formato, Map<String, Object> filtros, List<?> datos) {

    boolean todoBien = true;
    try {
      String idioma = (String) filtros.get("idiomaUsiario");

      if (Utilidades.isValidString(formato)) {
        byte[] contenido = null;
        String mimeType;
        switch (formato.toLowerCase()) {
          case "pdf":
            contenido = generaPDF(nomFichJRXML, filtros, datos, idioma);
            mimeType = "application/pdf";
            Filedownload.save(contenido, mimeType, nomFichSalida + ".pdf");
            break;

          case "ods":
            contenido = generaODS(nomFichJRXML, filtros, datos, idioma);
            mimeType = "application/vnd.oasis.opendocument.spreadsheet";
            Filedownload.save(contenido, mimeType, nomFichSalida + ".ods");
            break;

          case "xls":
            contenido = generaXLS(nomFichJRXML, filtros, datos, idioma);
            mimeType = "application/vnd.ms-excel";
            Filedownload.save(contenido, mimeType, nomFichSalida + ".xls");
            break;

          case "csv":
            contenido = generaCSV(nomFichJRXML, filtros, datos, idioma);
            mimeType = "text/csv";
            Filedownload.save(contenido, mimeType, nomFichSalida + ".csv");
            break;

          default:
            todoBien = false;
            break;
        }
      }
    } catch (JRException e) {
      log.error(e.getMessage());
      todoBien = false;
    }
    return todoBien;
  }
  
}
