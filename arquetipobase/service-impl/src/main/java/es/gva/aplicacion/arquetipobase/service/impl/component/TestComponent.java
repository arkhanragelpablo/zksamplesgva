package es.gva.aplicacion.arquetipobase.service.impl.component;

/**
 * Componentes: logica interna capa de negocio reusable desde otros services/componentes.
 * 
 * 
 * @author Indra
 *
 */
public interface TestComponent {

	void test();
	
}
