package es.gva.aplicacion.arquetipobase.service.impl.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TestComponentImpl implements TestComponent{

	private final Logger log = LoggerFactory.getLogger(TestComponentImpl.class);
	
	@Override
	public void test() {
		log.debug("test component");
	}

}
