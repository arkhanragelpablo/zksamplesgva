package es.gva.aplicacion.arquetipobase.service.impl.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Logger excepciones capa de negocio.
 * 
 * @author Indra
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NegocioInterceptor {

}
