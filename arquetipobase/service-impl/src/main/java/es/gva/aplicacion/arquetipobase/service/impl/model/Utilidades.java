package es.gva.aplicacion.arquetipobase.service.impl.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Utilidades.
 * 
 * @author Indra
 * 
 */
public final class Utilidades {

  /**
   * Atributo constante LOG de GseUtils.
   */
  private static final Logger LOG = LoggerFactory.getLogger(Utilidades.class);

  public static final String FORMATO_HORAS = "dd/MM/yyyy HH:mm";

  /** Formato date con fecha y hora. **/
  public static final String FORMAT_DATE_FECHA_HORA = "dd/MM/yyyy HH:mm";

  /** Formato date con fecha y hora. **/
  public static final String FORMAT_DATE_FECHA_HORA_SIN_ANYO = "dd/MM HH:mm";

  /** Formato date con fecha y sin hora. **/
  public static final String FORMAT_DATE_FECHA = "dd/MM/yyyy";

  /**
   * Atributo LETRAS.
   */
  public static final String LETRAS = "TRWAGMYFPDXBNJZSQVHLCKE";

  /**
   * Atributo SI n_ nif.
   */
  public static final String SIN_NIF = "[0-9]{0,8}[" + LETRAS + "]{1}";

  /**
   * Atributo SI n_ nie.
   */
  public static final String SIN_NIE = "[X|Y|Z][0-9]{1,8}[A-Z]{1}";

  /**
   * Constructor.
   */
  private Utilidades() {}



  /**
   * Dado un nombre de propiedad, la intenta recuperar como propiedad de la
   * maquina virtual java y si no existe, la busca como propiedad del entorno
   * 
   * @param propertyName
   * @return
   */
  public static String getPropiedad(String propertyName) {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Buscamos propiedad " + propertyName
          + " en las propiedades de la jvm...");
    }
    String resultado = System.getProperty(propertyName);
    if (resultado == null || resultado.trim().length() == 0) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Buscamos propiedad " + propertyName
            + " en el entorno del servidor...");
      }

      resultado = System.getenv(propertyName);
    }
    if (LOG.isDebugEnabled()) {
      LOG.debug("La propiedad " + propertyName + " es " + resultado);
    }

    return (resultado == null ? resultado : resultado.trim());
  }


  /**
   * Convierte date en string.
   * 
   * @param fecha Parámetro fecha
   * @param pFormato Parámetro formato
   * @return el string
   */
  public static String getHoraString(final Date fecha, final String pFormato) {
    String resultado = null;
    String formato = pFormato;
    if (fecha != null) {
      if (formato == null) {
        formato = "hh:mm:ss";
      }
      final SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.ITALY);
      resultado = sdf.format(fecha);
    }
    return resultado;
  }

  /**
   * Convierte date en string.
   * 
   * @param fecha Parámetro fecha
   * @param pFormato Parámetro formato
   * @return el string
   */
  public static String dateToString(final Date fecha, final String pFormato) {
    String resultado = null;
    String formato = pFormato;
    if (fecha != null) {
      if (formato == null) {
        formato = FORMAT_DATE_FECHA_HORA;
      }
      final SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.ITALY);
      resultado = sdf.format(fecha);
    }
    return resultado;
  }

  /**
   * Convierte date en string.
   * 
   * @param fecha Parámetro fecha
   * @param pFormato Parámetro formato
   * @return el string
   */
  public static String timestampToString(final Timestamp fecha,
      final String pFormato) {
    String resultado = null;
    String formato = pFormato;
    if (!Utilidades.isNull(fecha)) {
      if (Utilidades.isNull(formato)) {
        formato = FORMAT_DATE_FECHA_HORA;
      }
      final SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.ITALY);
      resultado = sdf.format(fecha);
    }
    return resultado;
  }

  /**
   * Acorta descripcion al tamaño indicado. Si tiene tamaño mayor acorta
   * poniendo al final puntos suspensivos (...).
   * 
   * @param descripcion Descripcion
   * @param tamMax Tamaño maximo
   * @return descripcion acortada
   */
  public static String acortarDescripcion(final String descripcion,
      final int tamMax) {
    String res = descripcion;
    if (res != null && res.length() > tamMax && res.length() > 3) {
      res = res.substring(0, tamMax - 3) + "...";
    }
    return res;
  }

  /**
   * Establece la hora a las 00:00:00 horas.
   * 
   * @param fecha Fecha
   * @return Fecha establecida a primera hora
   **/
  public static Date obtenerPrimeraHora(final Date fecha) {
    Date respuesta = null;
    if (fecha != null) {
      final Calendar calendario2 = Calendar.getInstance();
      calendario2.setTime(fecha);
      calendario2.set(Calendar.HOUR_OF_DAY, 0);
      calendario2.set(Calendar.MINUTE, 0);
      calendario2.set(Calendar.SECOND, 0);
      calendario2.set(Calendar.MILLISECOND, 0);
      respuesta = calendario2.getTime();
    }
    return respuesta;
  }

  /**
   * Establece la hora a las 23:59:59 horas.
   * 
   * @param fecha Fecha
   * @return Fecha establecida a primera hora
   **/
  public static Date obtenerUltimaHora(final Date fecha) {
    Date respuesta = null;
    if (fecha != null) {
      final Calendar calendario2 = Calendar.getInstance();
      calendario2.setTime(fecha);
      calendario2.set(Calendar.HOUR_OF_DAY, 23);
      calendario2.set(Calendar.MINUTE, 59);
      calendario2.set(Calendar.SECOND, 59);
      calendario2.set(Calendar.MILLISECOND, 0);
      respuesta = calendario2.getTime();
    }
    return respuesta;
  }

  /**
   * Convierte array de Strings a List de Long.
   * 
   * @param stringArray array de strings
   * @return List de Long
   */
  public static List<Long> stringArrayToListLong(final String[] stringArray) {
    final List<Long> res = new ArrayList<Long>();
    for (final String s : stringArray) {
      res.add(Long.valueOf(s));
    }
    return res;
  }

  /**
   * Valida si el par�metro String es valido (No es nulo y tiene contenido).
   * 
   * @param token String a Validar
   * @return si es valido o no
   */
  public static boolean isValidString(final String token) {
    boolean resultado;
    if (isNull(token)) {
      resultado = false;
    } else if (token.length() > 0) {
      resultado = true;
    } else {
      resultado = false;
    }
    return resultado;
  }

  /**
   * Valida si el objeto Integer pasado es valido (No es nulo y tiene
   * contenido).
   * 
   * @param token Integer a Validar
   * @return si es valido o no
   */
  public static boolean isValidInteger(final Integer token) {
    boolean resultado = false;
    if (!isNull(token) && token.intValue() > 0) {
      resultado = true;
    }
    return resultado;
  }

  /**
   * Método para Comprueba si es valid integer de la clase GseUtils.
   * 
   * @param token Parámetro token
   * @return el integer
   */
  public static Integer isValidInteger(final String token) {
    Integer resultado = null;
    if (!isNull(token)) {
      try {
        resultado = Integer.valueOf(token);
      } catch (final NumberFormatException nfe) {
        LOG.debug("Error convirtiendo el integer. Token:" + token);
      }
    }
    return resultado;
  }

  /**
   * Valida si el objeto Long pasado es valido (No es nulo y tiene contenido).
   * 
   * @param token Long a Validar
   * @return si es valido o no
   */
  public static boolean isValidLong(final Long token) {
    boolean resultado = false;
    if (isNull(token) && token > 0) {
      resultado = true;
    }
    return resultado;
  }

  /**
   * Método para Comprueba si es valid long de la clase GseUtils.
   * 
   * @param token Parámetro token
   * @return el long
   */
  public static Long isValidLong(final String token) {
    Long resultado = null;
    if (!isNull(token)) {
      try {
        resultado = Long.valueOf(token);
      } catch (final NumberFormatException nfe) {
        resultado = null;
      }
    }
    return resultado;
  }

  /**
   * Valida si el objeto pasado es nulo.
   * 
   * @param nulo Objeto a Validar
   * @return si es válido o no
   */
  public static boolean isNull(final Object nulo) {
    boolean resultado = false;
    if (nulo == null) {
      resultado = true;
    }
    return resultado;

  }

  /**
   * Valida si el objeto pasado no es nulo.
   * 
   * @param nulo Objeto a Validar
   * @return si es valido o no
   */
  public static boolean isNotNull(final Object nulo) {
    boolean resultado = true;
    if (isNull(nulo)) {
      resultado = false;
    }
    return resultado;
  }

  /**
   * Valida si el objeto List pasado es valido (No es nulo y tiene contenido).
   * 
   * @param token Lista a Validar
   * @return si es valido o no
   */
  public static boolean isValidList(final List<?> token) {
    boolean resultado = false;
    if (!isNull(token) && !token.isEmpty()) {
      resultado = true;
    }
    return resultado;
  }

  /**
   * Valida si el objeto Set pasado es valido (No es nulo y tiene contenido).
   * 
   * @param token Set a Validar
   * @return si es valido o no
   */
  public static boolean isValidSet(final Set<?> token) {
    boolean resultado = false;
    if (!isNull(token) && !token.isEmpty()) {
      resultado = true;
    }
    return resultado;
  }

  /**
   * Compara si el valor de dos Long son iguales.
   * 
   * @param valora Long a Comparar
   * @param valorb Long a Comparar
   * @return true si son iguales false si no son iguales
   */
  public static boolean sonIguales(final Date valora, final Date valorb) {
    boolean salida = false;
    if (isNotNull(valora) && isNotNull(valorb)) {
      salida = (valora.compareTo(valorb) == 0);
    }

    return salida;
  }

  /**
   * Compara si el valor de dos Long son iguales.
   * 
   * @param valora Long a Comparar
   * @param valorb Long a Comparar
   * @return true si son iguales false si no son iguales
   */
  public static boolean sonIguales(final Long valora, final Long valorb) {
    return (valora.compareTo(valorb) == 0);
  }

  /**
   * Compara si el valor de dos long son iguales.
   * 
   * @param valora long a Comparar
   * @param valorb long a Comparar
   * @return true si son iguales false si no son iguales
   */
  public static boolean sonIguales(final long valora, final long valorb) {
    return sonIguales(Long.valueOf(valora), Long.valueOf(valorb));
  }

  /**
   * Compara si el valor de dos int son iguales.
   * 
   * @param valora int a Comparar
   * @param valorb int a Comparar
   * @return true si son iguales false si no son iguales
   */
  public static boolean sonIguales(final int valora, final int valorb) {
    return sonIguales(Long.valueOf(valora), Long.valueOf(valorb));
  }

  /**
   * Valida si el objeto de tipo Collection pasado es valido (No es nulo y tiene
   * contenido).
   * 
   * @param cll Collection a Validar
   * @return si es valido o no
   */
  public static boolean isValidCollection(Collection<?> cll) {
    boolean resultado = false;
    // jnavarrov pongo el try-catch ya que fallaba el size a veces siendo
    // este
    // no nulo
    try {
      if (!cll.isEmpty()) {
        resultado = true;
      }
    } catch (Exception e) {
      LOG.debug("     Error en isValidCollection. Error :" + e.getMessage(), e);
    }
    return resultado;
  }

  /**
   * Metodo encargado de realizar casting a String
   * 
   * @param elemento Objeto al que hay que hacer el casting
   * 
   * @return Devuelve String o null si no se puede hacer el casting a String del
   *         objeto
   */
  public static String getCastingAString(final Object elemento) {
    String elementoString = Constantes.VACIO;
    if (elemento instanceof String) {
      elementoString = (String) elemento;
    } else if (elemento instanceof Date) {
      elementoString = Utilidades.dateToString((Date) elemento);
    } else if (elemento != null) {
      elementoString = String.valueOf(elemento);
    }
    return elementoString;
  }

  /**
   * Metodo encargado de realizar casting a Boolean
   * 
   * @param elemento Objeto al que hay que hacer el casting
   * 
   * @return Devuelve Boolean o null si no se puede hacer el casting a Boolean
   *         del objeto
   */
  public static Boolean getCastingABoolean(final Object elemento) {
    // jnavarrov trato el valor null como FALSE
    Boolean elementoBoolean = Boolean.FALSE;
    if (elemento instanceof Boolean) {
      elementoBoolean = (Boolean) elemento;
    }
    return elementoBoolean;
  }

  /**
   * Metodo encargado de realizar casting a Date
   * 
   * @param elemento Objeto al que hay que hacer el casting
   * 
   * @return Devuelve Date o null si no se puede hacer el casting a Date del
   *         objeto
   */
  public static Date getCastingADate(final Object elemento) {
    Date elementoDate = null;
    if (elemento instanceof Date) {
      elementoDate = (Date) elemento;
    }
    return elementoDate;
  }

  /**
   * Método para Retorna conexion criteria de la clase UsuarioDaoImpl.
   * 
   * @param hql Parámetro hql
   * @return el string
   */
  public static String retornaConexionHQL(final StringBuilder hql) {
    String salida;
    if (hql.lastIndexOf("where") < 0) {
      salida = " where ";
    } else {
      salida = " and ";
    }
    return salida;
  }

  /**
   * Método para Retorna conexion criteria de la clase UsuarioDaoImpl.
   * 
   * @param hql Parámetro hql
   * @return el string
   */
  public static String retornaConexionHQL(final StringBuilder hql,
      final boolean valoror) {
    String salida = retornaConexionHQL(hql);

    if (valoror && (salida.matches("and"))) {
      salida = salida.replaceAll("and", "or");
    }

    return salida;
  }

  /**
   * Transforma la fecha de un string a un Date.
   * 
   * @param fecha Fecha a transformar
   * @return String representando la fecha
   */
  public static String dateToString(final Date fecha) {
    String resultado = null;
    if (!isNull(fecha)) {
      final SimpleDateFormat sdf =
          new SimpleDateFormat(FORMATO_HORAS, Locale.ITALY);
      resultado = sdf.format(fecha);
    }
    return resultado;
  }

  /**
   * Transforma la fecha de un Date a String.
   * 
   * @param fecha Fecha a transformar
   * @return String representando la fecha
   */
  public static Date stringToDate(final String fecha) {
    Date resultado = null;
    if (!isNull(fecha)) {
      final SimpleDateFormat sdf =
          new SimpleDateFormat(FORMAT_DATE_FECHA, Locale.ITALY);
      try {
        resultado = sdf.parse(fecha);
      } catch (ParseException e) {
        resultado = null;
      }
    }
    return resultado;
  }

  /**
   * Transforma la fecha de un string a un Date corto.
   * 
   * @param fecha Fecha a transformar
   * @return String representando la fecha
   */
  public static String shortDateToString(final Date fecha) {
    String resultado = null;
    if (!isNull(fecha)) {
      final SimpleDateFormat sdf =
          new SimpleDateFormat(FORMAT_DATE_FECHA, Locale.ITALY);
      resultado = sdf.format(fecha);
    }
    return resultado;
  }

  /**
   * Devuelve la letra de control del NIF completo a partir de un DNI.
   * 
   * @param dniNumerico dni al que se quiere añadir la letra del NIF
   * @return el atributo letra nif Letra control NIF.
   */
  private static String getLetraNIF(final String dniNumerico) {
    final int dni = Integer.parseInt(dniNumerico, 10);
    final int modulo = dni % 23;

    final char letra = LETRAS.charAt(modulo);
    return String.valueOf(letra);

  }

  /**
   * Valida si el parametro es un NIF o no. Valida que: -- El parametro tenga 9
   * caracteres. -- El parametro termina en una letra. -- La letra cumple la
   * validación.
   * 
   * @param valor nif que se quiere validar
   * @return true, si es satisfactorio true Si es un NIF valido false Si no es
   *         un NIF valido
   */
  public static boolean esNif(final String valor) {
    boolean resultado = true;

    try {
      if (!isValidString(valor)) {
        resultado = false;
      }

      if (resultado
          && !Pattern.matches(SIN_NIF, valor.toUpperCase(Locale.ITALY))) {
        resultado = false;
      }

      if (resultado && ((valor.length() < 9) || (valor.length() > 10))) {
        resultado = false;
      }

      if (resultado) {
        final String letraNif = valor.substring(8).toUpperCase(Locale.ITALY);

        final StringBuffer digitos = new StringBuffer();

        for (int i = 0; i < valor.length(); i++) {
          final char character = valor.substring(i, i + 1).toCharArray()[0];
          if (Character.isDigit(character)) {
            digitos.append(character);
          }
        }

        final String letra = getLetraNIF(digitos.toString());

        resultado = letraNif.equals(letra);

      }

    } catch (final IllegalArgumentException exception) {
      LOG.error(exception.getMessage(), exception);
      resultado = false;
    }
    return resultado;

  }

  /**
   * Método para Es nif nie valido de GseUtils.
   * 
   * @param valor Parámetro valor
   * @return true, si es satisfactorio
   */
  public static boolean esEmailValido(final String valor) {

    final Pattern patronEmail =
        Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$");
    final Matcher m = patronEmail.matcher(valor);

    return (Utilidades.isNotNull(valor) && m.matches());
  }



  /**
   * Método para Es nif nie valido de GseUtils.
   * 
   * @param valor Parámetro valor
   * @return true, si es satisfactorio
   */
  public static boolean castBoolean(final Boolean valor) {

    return (Utilidades.isNotNull(valor) && valor.booleanValue());
  }

  // Cadena de caracteres original a sustituir.
  private static final String ORIGINAL = "áéíóúàèìòùÁÉÍÓÚÀÈÌÒÙ";
  // Cadena de caracteres ASCII que reemplazarán los originales.
  private static final String ASCII = "aeiouaeiouAEIOUAEIOU";

  /**
   * Método para Normalizar cadena de la clase GseUtils.
   * 
   * @param input Parámetro input
   * @param mayusculas Parámetro mayusculas
   * @return el string
   */
  public static String normalizarCadena(final String input,
      final boolean mayusculas) {
    String output = input;
    for (int i = 0; i < ORIGINAL.length(); i++) {
      // Reemplazamos los caracteres especiales.
      output = output.replace(ORIGINAL.charAt(i), ASCII.charAt(i));
    } // for i
    if (mayusculas) {
      output = output.toUpperCase(Locale.ITALY);
    } else {
      output = output.toLowerCase(Locale.ITALY);
    }
    return output;
  }

  /**
   * Método para Corta string de la clase GseUtils.
   * 
   * @param cadena Parámetro cadena
   * @param longitudCorte Parámetro longitud corte
   * @return el string
   */
  public static String cortaString(final String cadena,
      final int longitudCorte) {
    String corte = Constantes.VACIO;
    if (cadena.length() > longitudCorte) {
      corte = cadena.substring(0, longitudCorte) + "...";
    } else {
      corte = cadena;
    }
    return corte;
  }

  /**
   * Devuelve el estring con la condición HQL like
   * 
   * @param atributo
   * @param valor
   * @return String de la condición HQL
   */
  public static String daCondionLikeHQL(final String atributo,
      final String valor) {

    final StringBuilder condicion = new StringBuilder(" upper(");
    condicion.append(atributo);
    condicion.append(") like upper('%");
    condicion.append(valor);
    condicion.append("%') ");

    return condicion.toString();
  }

  /**
   * Devuelve el estring con la condición HQL like
   * 
   * @param atributo
   * @param valor
   * @return String de la condición HQL
   */
  public static String daCondionEqHQL(final String atributo,
      final String valor) {

    final StringBuilder condicion = new StringBuilder(" upper(");
    condicion.append(atributo);
    condicion.append(") = upper('");
    condicion.append(valor);
    condicion.append("') ");

    return condicion.toString();
  }

  /**
   * Devuelve el estring con la condición HQL like
   * 
   * @param atributo
   * @param valor
   * @return String de la condición HQL
   */
  public static String daCondionEqNumberHQL(final String atributo,
      final String valor) {

    final StringBuilder condicion = new StringBuilder(Constantes.ESPACIO);
    condicion.append(atributo);
    condicion.append(" = ");
    condicion.append(valor);
    condicion.append(Constantes.ESPACIO);

    return condicion.toString();
  }

  /**
   * Método que obtiene una copia de la fecha. Si es nula, lo devuelve como
   * null.
   * 
   * @param fechaEntrada
   * @return
   */
  public static Date getFechaCorrectamente(final Date fechaEntrada) {
    Date fechaRetorno;
    if (fechaEntrada == null) {
      fechaRetorno = null;
    } else {
      fechaRetorno = new Date(fechaEntrada.getTime());
    }
    return fechaRetorno;
  }

  /**
   * Método que obtiene una copia de la fecha. Si es nula, lo devuelve como
   * null.
   * 
   * @param fechaEntrada
   * @return
   */
  public static Timestamp getFechaCorrectamente(final Timestamp fechaEntrada) {
    Timestamp fechaRetorno;
    if (fechaEntrada == null) {
      fechaRetorno = null;
    } else {
      fechaRetorno = new Timestamp(fechaEntrada.getTime());
    }
    return fechaRetorno;
  }

  /**
   * Devuelve el texto pero con espacio por detrás suficiente para cumplir la
   * longitud.
   * 
   * @param texto
   * @param longitud
   * @return
   */
  public static String fit(final String texto, final int longitud) {
    final String retorno;
    final int tamanyoEspacio = longitud - texto.length();
    if (tamanyoEspacio < 0) { // Texto mayor que la longitud
      retorno = texto.substring(0, longitud);
    } else {
      final StringBuffer stringBuffer = new StringBuffer();
      for (int i = 0; i < tamanyoEspacio; i++) {
        stringBuffer.append(' ');
      }
      retorno = texto + stringBuffer.toString();
    }
    return retorno;
  }

  /**
   * Devuelve si el idioma de la sesión es en valenciano.
   * 
   * @param idioma
   * @return
   */
  public static boolean esValenciano(final String idioma) {

    boolean esV = false;

    if (Utilidades.isNotNull(idioma)
        && "VA".equalsIgnoreCase(idioma.toUpperCase())) {
      esV = true;
    }
    return esV;
  }

  /**
   * Devuelve la fecha pero en formato español, es decir, con los meses en
   * literal.
   * 
   * @param date
   * @return
   */
  public static String dateToStringESP(final Date fecha) {
    String resultado = null;
    final String formato = FORMAT_DATE_FECHA;
    if (fecha != null) {
      final SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.ITALY);
      final String[] resultadoSplit = sdf.format(fecha).split("/");
      if (resultadoSplit[1] == null) {
        resultado = sdf.format(fecha);
      } else {
        final StringBuffer texto = new StringBuffer();
        texto.append(resultadoSplit[0]);
        if ("01".equals(resultadoSplit[1])) {
          texto.append("ENERO");
        } else if ("02".equals(resultadoSplit[1])) {
          texto.append("FEBRERO");
        } else if ("03".equals(resultadoSplit[1])) {
          texto.append("MARZO");
        } else if ("04".equals(resultadoSplit[1])) {
          texto.append("ABRIL");
        } else if ("05".equals(resultadoSplit[1])) {
          texto.append("MAYO");
        } else if ("06".equals(resultadoSplit[1])) {
          texto.append("JUNIO");
        } else if ("07".equals(resultadoSplit[1])) {
          texto.append("JULIO");
        } else if ("08".equals(resultadoSplit[1])) {
          texto.append("AGOSTO");
        } else if ("09".equals(resultadoSplit[1])) {
          texto.append("SEPTIEMBRE");
        } else if ("10".equals(resultadoSplit[1])) {
          texto.append("OCTUBRE");
        } else if ("11".equals(resultadoSplit[1])) {
          texto.append("NOVIEMBRE");
        } else if ("12".equals(resultadoSplit[1])) {
          texto.append("DICIEMBRE");
        }
        texto.append(resultadoSplit[2]);
        resultado = texto.toString();
      }
    }
    return resultado;

  }

  /**
   * Obtiene el entero a partir de un string y de posición de inicio y fin. Si
   * no hay valor, devuelve el valor 0.
   * 
   * @param reg
   * @param i
   * @param j
   * @return
   */
  public static int parseInt(final String reg, final int i, final int j) {
    String data = reg.substring(i, i + j);
    data = data.trim();
    int valor;
    if ("".equals(data)) {
      valor = 0;
    }
    try {
      valor = Integer.parseInt(data);
    } catch (NumberFormatException nfe) {
      LOG.error("Error parseando entero. Mensaje:" + nfe.getMessage(), nfe);
      throw nfe;
    }
    return valor;
  }

  /**
   * Obtiene el float a partir de un string y de posición de inicio y fin. Si no
   * hay valor, devuelve el valor 0.
   * 
   * @param reg
   * @param i
   * @param j
   * @return
   */
  public static float parseFloat(final String reg, final int i, final int j) {
    String data = reg.substring(i, i + j);
    data = data.trim();
    float valor;
    if (data.length() == 0) {
      valor = 0;
    }
    try {
      valor = Float.parseFloat(data);
    } catch (NumberFormatException nfe) {
      LOG.error("Error parseando float. Mensaje:" + nfe.getMessage(), nfe);
      throw nfe;
    }
    return valor;
  }

  /**
   * (No funciona, comprobado y nada) Valida si un IBAN es correcto (sólo
   * español). Para validar hay que seguir los siguientes pasos:<br />
   * <br />
   * When you need to construct/deconstruct the Spanish Iban, spain is a country
   * which has two sets of check digits, calculated in the order below: 45, 91
   * <br />
   * <br />
   * ES91 2100 0418 4502 0005 1332<br />
   * <br />
   * Countrycode (2 characters 'ES') Check digits (2 characters) Bank identifier
   * (4 characters) Branch code (4 characters) check digit (2 characters)
   * Account number (10 characters) Step-by-step guide 1. First, change the ES
   * to its numeric equivalent E=14, S=28 and append this value with 00, so
   * 142800 Now, to get the first set of iban check digits, add account number
   * to the number from step 1: 0200051332142800
   * <ul>
   * <li>Now mod 97 that value, then subtract the remainder from 98.
   * 0200051332142800 mod 97 = 53, 98 - 53 = 45 <- first set of check digits!
   * (smile)</li>
   * <li>If you end up with one digit, it should be padded with a leading
   * zero.</li>
   * <li>To get the second iban check digits, append the bank id(2100) to the
   * branch id(0418) then account number, plus first check digits
   * (450200051332)then the value from step 1, in total:
   * 21000418450200051332142800</li>
   * <li>Again mod 97 that value, 21000418450200051332 mod 97 = 7, 98 -7 = 91 <-
   * second set of check digits (smile)</li>
   * </ul>
   * 
   * @param cuenta Tiene que tener los 24 digitos (empezando por ES). Ej:
   *        ES9034258324029250165663
   * @return
   * @seealso http://stackoverflow.com/questions/19881332/how-can-i-calculate-an
   *          - iban-national-check-digit
   */
  public static Boolean esIBANerroneo(final String cuenta) {
    boolean retorno;
    if (!cuenta.startsWith("ES")) {
      // Si no empieza por ES, error
      retorno = false;
    } else {
      // / PRIMERA FASE
      // 10 ultimos digitos + 1428 (ES) + 00
      final Long primeraParte = Long.valueOf(cuenta.substring(14) + "142800");
      // Primeros digitos de comprobacion
      final Long firstCheckDigit = Long.valueOf(cuenta.substring(12, 14));
      // Modulo con 97
      final Long modulo = primeraParte % 97l;
      // Restando 98
      final Long resultadoFirstCheckDigit = 98l - modulo;
      if (resultadoFirstCheckDigit.compareTo(firstCheckDigit) == 0) {
        // /////// SEGUNDA FASE /////////
        // Todos los dígitos menos los 4 primeros y 1280(ES) + 00
        final BigInteger segundaParte =
            new BigInteger(cuenta.substring(4) + "142800");
        // Primeros digitos de comprobacion
        final Long secondCheckDigit = Long.valueOf(cuenta.substring(2, 4));
        // Modulo con 97
        final BigInteger modulo2 = segundaParte.mod(new BigInteger("97"));
        // Restando 98
        final Long resultadoSecondCheckDigit = 98l - modulo2.longValue();

        if (resultadoSecondCheckDigit.compareTo(secondCheckDigit) == 0) {
          retorno = true;
        } else {
          retorno = false;
        }
      } else {
        retorno = false;
      }
    }

    return retorno;
  }

  /***
   * Parece ser que lo único que se tiene que hacer es convertir las 2 primeras
   * letras (en nuestro caso "ES" en nº, que sería el 1428 y concatenarlo al
   * final del nº de la cuenta y luego poner los 2 digitos que vienen con las
   * letras también al final. Si todo ese pedazo de nº al pasarle el modulo 97
   * da 1, entonces es que es correcto. <br />
   * Enlaces:
   * <ul>
   * <li>https://www.teslatechnologies.es/validar-iban-de-cuenta-bancaria-php/
   * </li>
   * <li>http://www.mclarenx.com/2014/02/04/validar-iban-en-javascript/</li>
   * </ul>
   * Comprobado con los siguientes iBAN (validados por la APP, por ESIRCA y por
   * http://iban.es/ ):
   * <ul>
   * <li>ES4020386533153000094575</li>
   * <li>ES6112311231121231231231</li>
   * <li>ES9121000418450200051332</li>
   * <li>ES0300811367310001010409</li>
   * <li>ES2364265841767173822054</li>
   * <li>ES9034258324029250165663</li>
   * </ul>
   * 
   * @param cuenta
   * @return
   */
  public static Boolean esIBAN(final String cuenta) {
    boolean resultado;
    try {
      final BigInteger segundaParte =
          new BigInteger(cuenta.substring(4) + "1428" + cuenta.substring(2, 4));
      final BigInteger modulo = segundaParte.mod(new BigInteger("97"));
      final Integer uno = 1;

      if (uno.compareTo(modulo.intValue()) == 0) {
        resultado = true;
      } else {
        resultado = false;
      }
    } catch (Exception exception) {
      LOG.debug("Error comprobando si es IBAN", exception);
      resultado = false;
    }
    return resultado;
  }

  /**
   * Convierte un bigDecimal en un String, con el nº de decimales que se desee.
   * 
   * @param nuevoValor Valor en bigDecimal que deseamos convertir a string.
   * @param numDecimales El nº de decimales máximo que se quiere mostrar.
   * @param mostrarCeros En caso de true se mostrarán siempre los 0 a la derecha
   *        del punto (aunque no sirvan) y en caso de false se quitarán.
   * @return
   */
  public static String bigDecimalToString(final BigDecimal valor,
      final int numDecimales, final boolean mostrarCeros) {
    String retorno = null;

    try {
      // Primero redondeamos.
      final BigDecimal nuevoValor =
          valor.setScale(numDecimales, BigDecimal.ROUND_DOWN);

      // Declaramos los parámetros de decimal format para convertir a un
      // string
      final DecimalFormat df = new DecimalFormat();

      // Nº de decimales máximos
      df.setMaximumFractionDigits(numDecimales);

      // Nº de decimales mínimos a mostrar
      if (mostrarCeros) {
        df.setMinimumFractionDigits(numDecimales);
      } else {
        df.setMinimumFractionDigits(0);
      }
      df.setGroupingUsed(false);
      // Devolver string a partir del bigDecimal con el DecimalFormat.
      retorno = df.format(nuevoValor); // .replace(",", ".");
    } catch (Exception e) {
      LOG.debug("Error convirtiendo a string el bigDecimal:" + valor, e);
    }

    return retorno;

  }

  /**
   * Convierte de texto a bigdecimal.
   * 
   * @param texto
   * @return
   */
  public static BigDecimal getCastingABigDecimal(final String texto,
      final boolean nuloAcero) {
    BigDecimal retorno;
    // Es simplemente por si está formateado, entonces hay que quitar los
    // puntos y la coma cambiarla por punto.
    String nuevoTexto = texto.replace(".", "");
    nuevoTexto = nuevoTexto.replace(",", ".");
    if (nuloAcero) {
      try {
        retorno = new BigDecimal(nuevoTexto);
      } catch (Exception exception) {
        LOG.debug("Error pero se ignora y se pone a 0", exception);
        retorno = BigDecimal.ZERO;
      }
    } else {
      retorno = new BigDecimal(nuevoTexto);
    }
    return retorno;
  }

  /**
   * Convierte un bigDecimal en un String, con el nº de decimales que se desee.
   * 
   * @param nuevoValor Valor en bigDecimal que deseamos convertir a string.
   * @param numDecimales El nº de decimales máximo que se quiere mostrar.
   * @param mostrarCeros En caso de true se mostrarán siempre los 0 a la derecha
   *        del punto (aunque no sirvan) y en caso de false se quitarán.
   * @return
   */
  public static String doubleToString(final Double valor,
      final int numDecimales, final boolean mostrarCeros) {
    String retorno = String.valueOf(valor);

    try {

      // Declaramos los parámetros de decimal format para convertir a un
      // string
      final DecimalFormat df = new DecimalFormat();

      // Nº de decimales máximos
      df.setMaximumFractionDigits(numDecimales);

      // Nº de decimales mínimos a mostrar
      if (mostrarCeros) {
        df.setMinimumFractionDigits(numDecimales);
      } else {
        df.setMinimumFractionDigits(0);
      }
      df.setGroupingUsed(false);
      // Devolver string a partir del bigDecimal con el DecimalFormat.
      retorno = df.format(valor); // .replace(",", ".");
    } catch (Exception e) {
      LOG.debug("Error convirtiendo a string el bigDecimal:" + valor, e);
    }

    return retorno;

  }

  /**
   * Convierte un texto en un bigdecimal, en caso de error, devuelv null.
   * 
   * @param texto
   * @return
   */
  public static BigDecimal getBigDecimal(final String texto) {
    BigDecimal retorno;
    try {
      retorno = new BigDecimal(texto);
    } catch (Exception e) {
      retorno = null;
    }
    return retorno;
  }

  /**
   * Convierte un double en un bigdecimal, en caso de error, devuelve null.
   * 
   * @param texto
   * @return
   */
  public static BigDecimal getBigDecimal(final Double texto) {
    BigDecimal retorno;
    try {
      retorno = new BigDecimal(texto);
    } catch (Exception e) {
      retorno = null;
    }
    return retorno;
  }

  /**
   * Obtiene el bigdecimal en string o, en caso de error, texto vacío.
   * 
   * @param string
   * @return
   */
  public static String bigDecimalToString(final BigDecimal bigDecimal) {
    String retorno;
    try {
      retorno = bigDecimal.toString();
    } catch (Exception e) {
      retorno = "";
    }
    return retorno;
  }

  /**
   * Verifica si el string pasado como parametros es fecha o no.
   * 
   * @param fecha
   * @return
   */
  public static boolean isFechaValida(String fecha) {
    try {
      SimpleDateFormat formatoFecha =
          new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
      formatoFecha.setLenient(false);
      formatoFecha.parse(fecha);
    } catch (ParseException e) {
      return false;
    }
    return true;
  }

  /**
   * Devuelve la parte derecha de la sentencia like
   * 
   * @param valor
   * @return %valor.toLowerCase%
   */
  public static String toValorCondicionLike(String valor) {
    StringBuilder resultado = new StringBuilder("");
    resultado.append("%");
    resultado.append(valor);
    resultado.append("%");
    return resultado.toString();
  }

  /**
   * Valida si el objeto es nulo, y en su caso nos devuelve el objeto sustituto.
   * 
   * @param a Objeto a valorar si es nulo
   * @param b Objeto susutituto en caso que a sea nulo
   * 
   * @return El objeto a si no es nulo, b en caso que a sea nulo
   */
  public <T> T nvl(T a, T b) {
    return (a == null) ? b : a;
  }

}
