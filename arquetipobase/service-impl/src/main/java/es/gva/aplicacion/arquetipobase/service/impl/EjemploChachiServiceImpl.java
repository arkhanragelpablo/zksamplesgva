package es.gva.aplicacion.arquetipobase.service.impl;

import es.gva.aplicacion.arquetipobase.service.api.EjemploChachiService;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.ChachiGridDTO;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.ContextoBusquedaDTO;
import es.gva.aplicacion.arquetipobase.service.api.model.comun.LineaListaDTO;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class EjemploChachiServiceImpl implements EjemploChachiService{

  @Override
  public ChachiGridDTO obtieneListaDatos(ContextoBusquedaDTO contextoBusqueda) {
    // TODO Auto-generated method stub
    
    ChachiGridDTO respuesta = new ChachiGridDTO();
    
    List<LineaListaDTO> datos = new ArrayList<LineaListaDTO> ();
    LineaListaDTO llDTO = null;
    
   
    
    for (int i = 0;i < 32;i++) {
        llDTO = new LineaListaDTO();
        llDTO.setCampoA("A" + String.format("%02d", i));
        llDTO.setCampoB("B" + String.format("%02d", i) );
        llDTO.setCampoC("C" + String.format("%02d", i) );
        llDTO.setCampoD("D" + String.format("%02d", i) );
        llDTO.setCampoE("E" + String.format("%02d", i) );
        llDTO.setCampoF("F" + String.format("%02d", i) );
        datos.add(llDTO);
    }
    

    int pageIni = (contextoBusqueda.getItemStartNumber()==0)?contextoBusqueda.getItemStartNumber():contextoBusqueda.getItemStartNumber()*contextoBusqueda.getPageSize();
    int pageFin = (pageIni + contextoBusqueda.getPageSize() > datos.size())?datos.size():pageIni + contextoBusqueda.getPageSize();
    
    if(contextoBusqueda.getColumnas()!=null && contextoBusqueda.getColumnas().size()>0) {
            
      
      if(contextoBusqueda.getColumnas().get(0).getFiltro()!=null) {
        List<Object> toRemove = new ArrayList<Object>();
        for(LineaListaDTO lldto : datos) {
           if (lldto.getCampoA().toLowerCase().indexOf(contextoBusqueda.getColumnas().get(0).getFiltro().toLowerCase())==-1){
             toRemove.add(lldto);
           }
        }
        datos.removeAll(toRemove);        
      }
      
      Comparator<LineaListaDTO> comparatorAux = (s1, s2) -> s1.getCampoA().compareTo(s2.getCampoA());
      
      if(!contextoBusqueda.getColumnas().get(0).isAscendente()) {
        datos.sort(comparatorAux.reversed());
      }else {
        datos.sort(comparatorAux);
      }
      
      
      if(contextoBusqueda.getColumnas().get(0).isAgrupado()) {
        int total = datos.size();
        
        datos = new ArrayList<LineaListaDTO> ();
        LineaListaDTO llDTOAux = new  LineaListaDTO();
        llDTOAux.setCampoA("A" + String.format("%02d", total));
        datos.add(llDTOAux);
      }
      
    }
    
 
    
    respuesta.setTotalSize(datos.size());
    
    if (datos.size()>= pageFin) {
      respuesta.setDatos( datos.subList(pageIni, pageFin));
    }else {
      respuesta.setDatos(datos);
    }
    
    
    
    return respuesta;
  }
}
